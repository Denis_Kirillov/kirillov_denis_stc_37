package ru.inno.game.client.Utils;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.util.Duration;
import ru.inno.game.client.controllers.MainController;
import ru.inno.game.client.socket.SocketClient;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class GameUtils {

    private static final int DAMAGE = 5;
    private static final int PLAYER_STEP = 5;
    private AnchorPane pane;
    private MainController mainController;
    private SocketClient socketClient;
    private final Lock lock = new ReentrantLock();
    private boolean isGameInProcess = true;


    public void goRight(Circle player) {
        double position = player.getCenterX() + PLAYER_STEP;

        if (position < 220) {
            player.setCenterX(position);
        }
    }

    public void goLeft(Circle player) {
        double position = player.getCenterX() - PLAYER_STEP;

        if (position > -220) {
            player.setCenterX(position);
        }
    }

    public Circle createBulletFor(Circle player, boolean isEnemy) {
        Circle bullet = new Circle();
        bullet.setRadius(5);
        pane.getChildren().add(bullet);
        bullet.setCenterX(player.getCenterX() + player.getLayoutX());
        bullet.setCenterY(player.getCenterY() + player.getLayoutY());
        bullet.setFill(Color.ORANGE);

        int value = resolveValue(isEnemy);
        final Circle target = resolveTarget(isEnemy);
        final Label targetHp = resolveTargetHp(isEnemy);

        Timeline timeLine = new Timeline(new KeyFrame(Duration.seconds(0.005), animation ->
                animationForBullet(isEnemy, bullet, value, target, targetHp)));
        timeLine.setCycleCount(500);
        timeLine.play();
        return bullet;
    }

    private void animationForBullet(boolean isEnemy, Circle bullet, int value, Circle target, Label targetHp) {
        bullet.setCenterY(bullet.getCenterY() + value);

        if (bullet.isVisible() && isIntersects(bullet, target)) {
            lock.lock();
            createDamage(targetHp);
            bullet.setVisible(false);
            lock.unlock();

            if (!isEnemy) {
                socketClient.sendMessage("Damage");
            }
        }

        if (isBulletOutside(bullet)) {
            pane.getChildren().remove(bullet);
        }
    }

    private int resolveValue(boolean isEnemy) {
        if (isEnemy) {
            return 1;
        } else {
            return -1;
        }
    }

    private Circle resolveTarget(boolean isEnemy) {
        if (!isEnemy) {
            return mainController.getEnemy();
        } else {
            return mainController.getPlayer();
        }
    }

    private Label resolveTargetHp(boolean isEnemy) {
        if (!isEnemy) {
            return mainController.getEnemyHp();
        } else {
            return mainController.getPlayerHp();
        }
    }

    private void createDamage(Label targetHp) {
        int health = Integer.parseInt(targetHp.getText()) - DAMAGE;

        if (health >= 0) {
            targetHp.setText(String.valueOf(health));

            if (health == 0) {
                socketClient.sendMessage("exit");
            }
        }
    }

    private boolean isBulletOutside(Circle bullet) {
        return bullet.getCenterY() < 30 || bullet.getCenterY() > 470;
    }

    private boolean isIntersects(Circle bullet, Circle target) {
        return bullet.getBoundsInParent().intersects(target.getBoundsInParent());
    }

    public void setPane(AnchorPane pane) {
        this.pane = pane;
    }

    public void setController(MainController mainController) {
        this.mainController = mainController;
    }

    public void setClient(SocketClient socketClient) {
        this.socketClient = socketClient;
    }

    public boolean getIsGameInProcess() {
        return isGameInProcess;
    }

    public void setGameInProcess(boolean gameInProcess) {
        isGameInProcess = gameInProcess;
    }
}
