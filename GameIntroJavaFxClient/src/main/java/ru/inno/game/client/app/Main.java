package ru.inno.game.client.app;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import ru.inno.game.client.controllers.MainController;

public class Main extends Application {
    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage stage) throws Exception {
        String fxmlFile = "/fxml/Main.fxml";
        FXMLLoader loader = new FXMLLoader();
        Parent root = loader.load(getClass().getResourceAsStream(fxmlFile));

        stage.setTitle("Game client");
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setResizable(false);

        MainController controller = loader.getController();
        scene.setOnKeyPressed(controller.getKeyEventEventHandler());

        stage.show();
    }
}
