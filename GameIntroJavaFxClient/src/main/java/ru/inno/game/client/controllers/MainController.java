package ru.inno.game.client.controllers;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Circle;
import ru.inno.game.client.Utils.GameUtils;
import ru.inno.game.client.socket.SocketClient;

import java.net.URL;
import java.util.ResourceBundle;

public class MainController implements Initializable {

    private GameUtils gameUtils;

    private SocketClient socketClient;
    @FXML
    private Circle player;

    @FXML
    private Circle enemy;

    @FXML
    private Button buttonConnect;

    @FXML
    private Button buttonGo;

    @FXML
    private TextField playerName;

    @FXML
    private AnchorPane pane;

    @FXML
    private Label playerHp;

    @FXML
    private Label enemyHp;

    @FXML
    private TextArea gameTextArea;


    public EventHandler<KeyEvent> getKeyEventEventHandler() {
        return keyEventEventHandler;
    }

    private EventHandler<KeyEvent> keyEventEventHandler = event -> {
        if (gameUtils.getIsGameInProcess()) {
            if (event.getCode() == KeyCode.RIGHT) {
                gameUtils.goRight(player);
                socketClient.sendMessage("right");
            } else if (event.getCode() == KeyCode.LEFT) {
                gameUtils.goLeft(player);
                socketClient.sendMessage("left");
            } else if (event.getCode() == KeyCode.SPACE) {
                Circle bullet = gameUtils.createBulletFor(player, false);
                socketClient.sendMessage("shot");
            }
        }
    };

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        gameUtils = new GameUtils();

        buttonConnect.setOnAction(event -> {
            socketClient = new SocketClient(this, "localhost", 7777);
            new Thread(socketClient).start();
            buttonConnect.setDisable(true);
            buttonGo.setDisable(false);
            playerName.setDisable(false);
            gameUtils.setPane(pane);
            gameUtils.setClient(socketClient);
        });

        buttonGo.setOnAction(event -> {
            socketClient.sendMessage("name: " + playerName.getText());
            buttonGo.setDisable(true);
            playerName.setDisable(true);
            buttonGo.getScene().getRoot().requestFocus();
        });

        gameUtils.setController(this);
    }

    public GameUtils getGameUtils() {
        return gameUtils;
    }

    public Circle getEnemy() {
        return enemy;
    }

    public Circle getPlayer() {
        return player;
    }

    public Label getPlayerHp() {
        return playerHp;
    }

    public Label getEnemyHp() {
        return enemyHp;
    }

    public TextArea getGameTextArea() {
        return gameTextArea;
    }
}
