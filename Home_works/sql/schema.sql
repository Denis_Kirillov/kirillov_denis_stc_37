create table player(
                       id bigserial primary key,
                       ip varchar(20),
                       name varchar(20),
                       points integer,
                       win_count integer,
                       lose_count integer
);

create table game(
                     id bigserial primary key,
                     time varchar(40),
                     first_player integer references player (id),
                     second_player integer references player (id),
                     first_player_shot_count integer,
                     second_player_shot_count integer,
                     amount_of_seconds integer

);

create table shot(
                     id bigserial primary key,
                     shot_time varchar(40),
                     game_id integer references game (id),
                     shooter integer references player (id),
                     target integer references player (id)
);


select *, g.id as game_id, f.id as f_id, f.ip as f_ip, f.points as f_points, f.win_count as f_wc, f.lose_count as f_lc
from game g left join player f on g.first_player = f.name left join player s on g.second_player = s.name;