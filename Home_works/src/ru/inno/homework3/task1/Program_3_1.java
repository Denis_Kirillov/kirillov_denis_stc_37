package ru.inno.homework3.task1;

import java.util.Scanner;
import java.util.Arrays;

class Program_3_1 {

	public static int[] getArray() {															// инициализация алгоритма объявления массива

		Scanner scanner = new Scanner(System.in);												
		System.out.print("Enter array size: ");
		int size = scanner.nextInt();															// Объявление размера массива
		int array[] = new int[size];															// Объявление массива

		System.out.println("Enter array elements: ");

		for (int i = 0; i < array.length; i++) {												// Считывание массива

			array[i] = scanner.nextInt();

		}

		return array;																			
	}


	public static void getArraySum(int[] array) {												// инициализация алгоритма поиска суммы элементов массива

		int arraySum = 0;


		for (int i = 0; i < array.length; i++) {

			arraySum += array[i]; 

		}

		System.out.println("Sum of array eliments: " + arraySum);
	}


	public static void getArrayAverage(int[] array) {											// инициализация алгоритма вычисления среднего

		int arraySum = 0;


		for (int i = 0; i < array.length; i++) {

			arraySum += array[i]; 

		}

		System.out.println("Average of array: " + (double) arraySum/array.length);
	}


	public static void arrayTurn(int[] array) {													// инициализация алгоритма разворота массива

		int arrayInv[] = new int[array.length];	

		
		for (int i = 0; i < array.length; i++) {

			arrayInv[i] = array[array.length - 1 - i];	

		}
		
		System.out.println("Turned array: " + Arrays.toString(arrayInv));
	}


	public static void arraySwap(int[] array) {													// инициализация алгоритма смены местами мин. и макс. элементов массива

		int arrayInv[] = new int[array.length];													// Объявление изменяемого массива
		int min = array[0];																		// Объявление минимума
		int minIndex = 0;																		// Объявление индекса минимума
		int max = array[0];																		// Объявление максимума
		int maxIndex = 0;																		// Объявление индекса максимума

		
		for (int i = 0; i < array.length; i++) {																		

			arrayInv[i] = array[i];		

		}


		for (int i = 0; i < array.length; i++) {												// Цикл поиска минимума и максимума

			if (arrayInv[i] > max) {

				max = arrayInv[i];
				maxIndex = i;

			}

			if (arrayInv[i] < min) {

				min = arrayInv[i];
				minIndex = i;

			}

		}

		arrayInv[maxIndex] = min;																// смена макс и мин местами
		arrayInv[minIndex] = max;

		System.out.println("Swapped array: " + Arrays.toString(arrayInv));
	}


	public static void bubbleSort(int[] array) {												// инициализация алгоритма сортировки массива пузырьком

		int arrayInv[] = new int[array.length];													// Объявление изменяемого массива
		int arrayBuf = 0;																		// Объявление буферной переменной дял сортировки


		for (int i = 0; i < array.length; i++) {

			arrayInv[i] = array[i];			
		}

		for (int i = 1; i < array.length; i++) {												// цикл сортировки

			for (int j = 0; j < array.length - i; j++) {

				if (arrayInv[j] > arrayInv[j+1]) {

					arrayBuf = arrayInv[j+1];						
					arrayInv[j+1] = arrayInv[j];
					arrayInv[j] = arrayBuf;

				}

			}

		}

		System.out.println("Sorted array: " + Arrays.toString(arrayInv));
		
	}


	public static void moveArrayToLong(int[] array) {											// инициализация алгоритма перевода массива в число						

		long number = 0L;
		int power = 0;														
		int arrayBuf = 0;


		for (int i = array.length-1; i >= 0; i--) {	

			arrayBuf = array[i];			
			number += (arrayBuf * (long) Math.pow(10,power));									// сложение чисел 

			while (arrayBuf / 10 > 0) {															// Определение дополнительных разрядов для чисел состоящих из 2 и более цифр

				power++;													
				arrayBuf /= 10;
			}

			power++;
		}

		System.out.println("Transformed array: " + number);
	}


	public static int helpMenu(int[] array) {													// инициализация пользовательского меню

		Scanner scanner = new Scanner(System.in);

		System.out.println("\n-------------------------------------------");
		System.out.println("Your array: " + Arrays.toString(array) + "\n");
		System.out.println("What to do with array? Choose from 1 to 7: ");
		System.out.println("1 - Count sum of array elements");
		System.out.println("2 - Count average of array elements");
		System.out.println("3 - Mirror array ");
		System.out.println("4 - Swap min and max elements of array ");
		System.out.println("5 - Sort array");
		System.out.println("6 - Move array to number");
		System.out.println("7 - Enter new array");
		System.out.println("0 - Exit");
		System.out.println("-------------------------------------------\n");	

		return scanner.nextInt();																// Возврат выбора пользователя

	}


	public static void main(String[] args) {													// начало MAIN 


		Scanner scanner = new Scanner(System.in);
		int array[] = getArray();																// Объявление массива


		int choiceValue = helpMenu(array);														// Пользовательский интерфейс и выбор действия

				
		while (choiceValue != 0) {																// Перебор выборки и вызов функций

			switch(choiceValue) {																

				case 1:

					getArraySum(array);															
					break;

				case 2:

					getArrayAverage(array);
					break;

				case 3:

					arrayTurn(array);
					break;

				case 4:

					arraySwap(array);
					break;

				case 5:	

					bubbleSort(array);
					break;

				case 6:

					moveArrayToLong(array);
					break;

				case 7:

					array = getArray();
					break;

				default:
					
					break;
			}

			choiceValue = helpMenu(array);														// Повтор выбора функций
	
		}
			
	}

}