package ru.inno.homework3.task2;

import java.util.Scanner;

class Program_3_2 {


	public static double f(double x) {													// Метод объявления функции

		return x * x;

	}


	public static double calcIntegral(double a, double b, int n) {						// Метод подсчета интеграла

		double h = (b - a) / n;															// Ширина разбиения dx
		double result = 0.0;

		for (double x = a + h; x <= b - h; x += 2 * h) {								// Цикл подсчета интеграла методом Симпсона

			result +=(h / 3) * (f(x - h) + (4 * f(x)) + f(x + h)); 

		}

		return result;

	}


	public static void showResult(double a, double b) {									// Метод вывода результатов для разного количества разбиений

		double result = 0;


		System.out.println("\nResults for different count of splitting: ");
		System.out.println("\n   Count   |      Result     |");

		for (int i = 10; i <= 100000; i *= 10) {

		result = calcIntegral(a, b, i);
		System.out.printf("\n%-10d | %15.11f |", i, result);

		}

	}


	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter lower limit a: ");
		double a = scanner.nextDouble();
		System.out.println("Enter upper limit b: ");
		double b = scanner.nextDouble();


		showResult(a, b);

	}
	
}