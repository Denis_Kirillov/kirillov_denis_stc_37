package ru.inno.homework11;
import java.util.Scanner;

public class UserUI {

    public void actionMenu() {

        System.out.println("\nChoose what to do with your list:");
        System.out.println("1 - Add next element");
        System.out.println("2 - Add element to begin");
        System.out.println("3 - Insert element by index");
        System.out.println("4 - Remove element by index");
        System.out.println("5 - Remove all coincidental elements");
        System.out.println("6 - Check for containing element");
        System.out.println("0 - Exit");

    }

    public void choiceListMenu() {

        System.out.println("\nChoose list:");
        System.out.println("1 - Arraylist");
        System.out.println("2 - Linkedlist");
        System.out.println("0 - Exit");

    }

    public void choiceList() {
        Scanner scanner = new Scanner(System.in);
        choiceListMenu();
        InnoList list;

        int choiceListValue = scanner.nextInt();

        if (choiceListValue == 1) {
            list = new InnoArrayList();
            actionList(list);
        } else if (choiceListValue == 2) {
            list = new InnoLinkedList();
            actionList(list);
        } else {
            System.out.println("Exit ...");
        }
    }

    public void result(InnoIterator iterator){

        System.out.println("Now your list is :\n");
        iterator.returnToBegin();


        while (iterator.hasNext()) {
            System.out.print(iterator.next() + " ");
        }
        System.out.println("\n");
    }


    public void actionList(InnoList list) {
        InnoIterator iterator = list.iterator();
        Scanner scanner = new Scanner(System.in);
        actionMenu();

        int choiceActionValue = scanner.nextInt();

        while (choiceActionValue != 0) {

            switch (choiceActionValue) {

                case 1:
                    System.out.println("Enter element to add");
                    list.add(scanner.nextInt());
                    result(iterator);

                    actionMenu();
                    choiceActionValue = scanner.nextInt();
                    break;

                case 2:
                    System.out.println("Enter element to add to begin");
                    list.addToBegin(scanner.nextInt());
                    result(iterator);

                    actionMenu();
                    choiceActionValue = scanner.nextInt();
                    break;

                case 3:
                    System.out.println("Enter index of element");
                    int index = scanner.nextInt();
                    System.out.println("Enter element to insert");
                    int element = scanner.nextInt();

                    list.insert(index, element);
                    result(iterator);

                    actionMenu();
                    choiceActionValue = scanner.nextInt();
                    break;

                case 4:
                    System.out.println("Enter index of element to remove");

                    list.removeByIndex( scanner.nextInt());
                    result(iterator);

                    actionMenu();
                    choiceActionValue = scanner.nextInt();
                    break;

                case 5:
                    System.out.println("Enter element to remove");

                    list.remove(scanner.nextInt());
                    result(iterator);

                    actionMenu();
                    choiceActionValue = scanner.nextInt();
                    break;

                case 6:
                    System.out.println("Enter element to check");

                    System.out.println("Is your list contains element: " + list.contains(scanner.nextInt()));

                    actionMenu();
                    choiceActionValue = scanner.nextInt();
                    break;

                default:
                    actionMenu();
                    choiceActionValue = scanner.nextInt();
                    break;
            }

        }

    }

}
