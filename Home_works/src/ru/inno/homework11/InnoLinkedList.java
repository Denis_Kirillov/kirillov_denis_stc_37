package ru.inno.homework11;

/**
 * 11.03.2021
 * 24. Collections
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// Плюсы - неограниченный размер (ограничен VM), быстрое добавление/удаление в начало/конец
// Минусы - медленный доступ по  индексу
public class InnoLinkedList implements InnoList {

    private static class Node {
        int value;
        Node next;

        Node(int value) {
            this.value = value;
        }
    }

    private Node first;
    private Node last;

    private int count;

    // get(7)
    @Override
    public int get(int index) {
        if (index >= 0 && index < count) {
            // начинаем с первого элемента
            Node current = first;
            // отсчитываем элементы с начала списка пока не дойдем до элемента с нужной позицией
            for (int i = 0; i < index; i++) {
                // переходим к следующему
                current = current.next;
            }
            // возвращаем значение
            return current.value;
        } else {
            return -1;
        }
    }

    @Override
    public void insert(int index, int element) {
        // TODO: уже реализовано!
        if (index >= 0 && index < count) {

            Node current = first;

            for (int i = 0; i < index + 1; i++) {

                if (i == index) {
                    current.value = element;
                    break;
                }

                current = current.next;
            }
        }

    }

    @Override
    public void addToBegin(int element) {
        // TODO: уже реализовано!
        Node newNode = new Node(element);

        if (first == null) {
            first = newNode;
        } else {
            newNode.next = first;                               // Устанавливаю старый первый узел следующим за новым
        }

        first = newNode;                                        // Устанавливаю новый узел первым
        count++;
    }

    @Override
    public void removeByIndex(int index) {
        // TODO: уже реализовано!

        Node current = first;
        Node previous = first;

        for (int i = 0; i < count; i++) {

            if (i == index) {

                if (current.equals(first)) {                   // переназначение первого узла если нужный элемент являлся первым
                    first = current.next;
                    break;
                } else {
                    previous.next = current.next;
                    break;
                }

            } else {
                previous = current;
            }

            current = current.next;
        }

        count--;                                               // обновление количества узлов в коллекции

    }


    @Override
    public void add(int element) {

        Node newNode = new Node(element);

        if (first == null) {
            first = newNode;
        } else {
            // следующий после последнего - новый узел
            last.next = newNode;
        }

        // новый узел - последний
        last = newNode;
        count++;
    }

    @Override
    public void remove(int element) {
        // TODO: уже реализовано!

        Node current = first;
        Node previous = first;
        int newCount = count;

        for (int i = 0; i < count; i++) {

            if (current.value == element) {

                if (current.equals(first)) {                   // переназначение первого узла если нужный элемент являлся первым
                    first = current.next;
                    previous = current.next;
                } else {
                    previous.next = current.next;
                }

                newCount--;

            } else {
                previous = current;
            }

            current = current.next;
        }

        count = newCount;                                       // обновление количества узлов в коллекции

    }

    @Override
    public boolean contains(int element) {
        // TODO: уже реализовано!

        Node current = first;

        for (int i = 0; i < count; i++) {

            if (current.value == element) {
                return true;
            }

            current = current.next;
        }
        return false;
    }


    @Override
    public int size() {
        return count;
    }

    @Override
    public InnoIterator iterator() {
        return new InnoLinkedListIterator();
    }

    private class InnoLinkedListIterator implements InnoIterator {

        // ссылка на текущий узел итератора
        private Node current;

        InnoLinkedListIterator() {
            this.current = first;
        }

        @Override
        public int next() {
            int nextValue = current.value;
            // сдвигаем указатель на следующий узел
            current = current.next;
            return nextValue;
        }

        @Override
        public boolean hasNext() {
            // если следующего узла нет - не идем дальше
            return current != null;
        }

        @Override
        public void returnToBegin() {
            this.current = first;
        }
    }
}
