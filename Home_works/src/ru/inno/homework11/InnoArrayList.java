package ru.inno.homework11;

/**
 * 11.03.2021
 * 24. Collections
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */

public class InnoArrayList implements InnoList {

    private static final int DEFAULT_SIZE = 10;

    private int elements[];

    private int count;

    public InnoArrayList() {
        this.elements = new int[DEFAULT_SIZE];
        this.count = 0;
    }

    @Override
    public int get(int index) {
        if (index >= 0 && index < count) {
            return elements[index];
        } else {
            return -1;
        }
    }

    @Override
    public void insert(int index, int element) {
        // TODO: уже реализовано!

        elements[index] = element;

    }

    @Override
    public void addToBegin(int element) {
        // TODO: уже реализовано!

        if (count == elements.length) {
            resize();
        }

        for (int i = count; i > 0; i--) {
            elements[i] = elements[i - 1];
        }

        elements[0] = element;
        count++;
    }

    @Override
    public void removeByIndex(int index) {
        // TODO: уже реализовано!
        int j = 0;

        for (int i = 0; i < count - j; i++) {

            if (i == index) {
                j++;
            }

            elements[i] = elements[i + j];
        }

        count--;
    }

    @Override
    public void add(int element) {
        // если список переполнен
        if (count == elements.length) {
            resize();
        }

        elements[count++] = element;
    }

    private void resize() {
        // создаем новый массив в полтора раза больший
        int newElements[] = new int[elements.length + elements.length / 2];
        // копируем из старого массива все элементы в новый
        for (int i = 0; i < count; i++) {
            newElements[i] = elements[i];
        }
        // устанавливаем ссылку на новый массив
        this.elements = newElements;
    }

    @Override
    public void remove(int element) {
        // TODO: уже реализовано!
        removeRecursion(0, elements, element);

    }

    public void removeRecursion(int index, int[] array, int element) {

        for (int i = index; i < array.length; i++) {

            if (array[i] == element) {

                for (int j = i; j < array.length - 1; j++) {
                    array[j] = array[j + 1];
                }

                count--;
                removeRecursion(i, array, element);                     //рекурсия начинается c i на случай, если
            }                                                           // j + 1 элемент тоже будет удаляемым
        }

    }

    @Override
    public boolean contains(int element) {
        // TODO: уже реализовано!

        for (int i = 0; i < count; i++) {
            if (elements[i] == element) {
                return true;
            }
        }

        return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public InnoIterator iterator() {
        // возвращаем новый экземпляр итератора
        return new InnoArrayListIterator();
    }

    // внутренний класс позволяет инкапсулировать логику одного класса внутри класса
    private class InnoArrayListIterator implements InnoIterator {
        // текущая позиция итератора
        private int currentPosition;

        @Override
        public int next() {
            // берем значение под текущей позицией итератора
            int nextValue = elements[currentPosition];
            // увеличиваем позицию итератора
            currentPosition++;
            // возвращаем значение
            return nextValue;
        }

        @Override
        public boolean hasNext() {
            // если текущая позиция не перевалила за общее количество элементов - можно дальше
            return currentPosition < count;
        }

        @Override
        public void returnToBegin() {
            this.currentPosition = 0;
        }
    }
}
