package ru.inno.homework9;

public interface NumberProcess {
    int process(int number);
}
