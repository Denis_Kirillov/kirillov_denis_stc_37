package ru.inno.homework9;

import java.util.Arrays;
import java.util.Scanner;

public class Program_9_1 {
    public static void main(String[] args) {

        inputMenu();

    }


    public static void helpMenuString(String[] string) {
        System.out.println("\nYour array is:" + Arrays.toString(string));
        System.out.println("Choose what to do :");
        System.out.println("1 - Turn strings");
        System.out.println("2 - Delete all digits");
        System.out.println("3 - Make characters uppercase");
        System.out.println("0 - Exit");

    }


    public static void helpMenuNumbers(int[] numbers) {

        System.out.println("\nYour array is:" + Arrays.toString(numbers));
        System.out.println("Choose what to do :");
        System.out.println("1 - Turn numbers");
        System.out.println("2 - Delete all zero digits");
        System.out.println("3 - Transform to even number");
        System.out.println("0 - Exit");

    }


    public static void inputMenu() {

        Scanner scanner = new Scanner(System.in);
        int inputChoice = 0;

        System.out.println("\nChoose array type:");
        System.out.println("1 - string");
        System.out.println("2 - numbers");
        inputChoice = scanner.nextInt();

        if (inputChoice == 1) {
            inputActions(inputString());
        } else if (inputChoice == 2) {
            inputActions(inputNumbers());
        } else {
            System.out.println("Incorrect input");
        }


    }


    public static String[] inputString() {

        Scanner scanner = new Scanner(System.in);                   // Второй сканер нужен, чтобы избежать проблемы с
        Scanner scanner2 = new Scanner(System.in);                  // присваиванием пустой строки в nextLine

        System.out.println("Enter string array length:");
        String[] string = new String[scanner.nextInt()];

        System.out.println("Enter your string array:");
        for (int i = 0; i < string.length; i++) {
            string[i] = scanner2.nextLine();
        }

        return string;
    }


    public static int[] inputNumbers() {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter numbers array length:");
        int[] numbers = new int[scanner.nextInt()];

        System.out.println("Enter your numbers array:");

        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = scanner.nextInt();
        }

        return numbers;
    }


    public static void inputActions(String[] inputString) {

        Scanner scanner = new Scanner(System.in);
        NumberAndStringProcessor stringArray = new NumberAndStringProcessor(inputString);
        StringProcess process;

        helpMenuString(inputString);
        int actionChoice = scanner.nextInt();

        while (actionChoice != 0) {

            switch (actionChoice) {

                case (1):
                    process = string -> {

                        char[] array = string.toCharArray();
                        char[] arrayInv = new char[array.length];

                        for (int i = array.length - 1; i > -1; i--) {
                            arrayInv[i] = array[array.length - 1 - i];
                        }

                        return new String(arrayInv);
                    };

                    stringArray.process(process);

                    helpMenuString(inputString);
                    actionChoice = scanner.nextInt();
                    break;

                case (2):
                    process = string -> {

                        int shiftValue = 0;
                        char[] numbersChar = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
                        char[] array = string.toCharArray();
                        char[] arrayNew = new char[array.length];

                        for (int i = 0; i < array.length; i++) {

                            int checkValue = 0;

                            for (char c : numbersChar) {
                                if (array[i] == c) {
                                    checkValue += 1;
                                }
                            }

                            if (checkValue > 0) {
                                shiftValue += 1;
                            } else {
                                arrayNew[i - shiftValue] = array[i];
                            }
                        }

                        return new String(arrayNew);
                    };

                    stringArray.process(process);

                    helpMenuString(inputString);
                    actionChoice = scanner.nextInt();
                    break;

                case (3):
                    process = string -> (string.toUpperCase());

                    stringArray.process(process);

                    helpMenuString(inputString);
                    actionChoice = scanner.nextInt();
                    break;

                default:
                    helpMenuString(inputString);
                    actionChoice = scanner.nextInt();
                    break;
            }

        }
    }

    public static void inputActions(int[] inputNumbers) {

        Scanner scanner = new Scanner(System.in);
        NumberAndStringProcessor numbersArray = new NumberAndStringProcessor(inputNumbers);
        NumberProcess process;

        helpMenuNumbers(inputNumbers);
        int actionChoice = scanner.nextInt();

        while (actionChoice != 0) {

            switch (actionChoice) {

                case (1):
                    process = number -> {

                        int power = Integer.toString(number).length() - 1;             // Определение количества цифр в числе
                        int numberInv = 0;

                        while (number != 0) {
                            numberInv += (number % 10) * Math.pow(10, power--);
                            number /= 10;
                        }

                        return numberInv;
                    };

                    numbersArray.process(process);

                    helpMenuNumbers(inputNumbers);
                    actionChoice = scanner.nextInt();
                    break;

                case (2):
                    process = number -> {

                        int power = 0;
                        int number2 = 0;

                        while (number != 0) {

                            if (number % 10 != 0) {
                                number2 += (number % 10) * Math.pow(10, power++);
                            }

                            number /= 10;
                        }

                        return number2;
                    };

                    numbersArray.process(process);

                    helpMenuNumbers(inputNumbers);
                    actionChoice = scanner.nextInt();
                    break;

                case (3):
                    process = number -> {

                        if (number % 2 != 0) {
                            number -= 1;
                        }

                        return number;
                    };

                    numbersArray.process(process);

                    helpMenuNumbers(inputNumbers);
                    actionChoice = scanner.nextInt();
                    break;


                default:
                    helpMenuNumbers(inputNumbers);
                    actionChoice = scanner.nextInt();
                    break;
            }

        }

    }
}
