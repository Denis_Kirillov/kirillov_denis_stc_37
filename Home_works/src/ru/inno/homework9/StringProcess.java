package ru.inno.homework9;

public interface StringProcess {
    String process(String string);
}
