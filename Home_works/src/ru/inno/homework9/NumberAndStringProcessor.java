package ru.inno.homework9;

import java.util.Arrays;

public class NumberAndStringProcessor {

    private String[] arrayString;
    private int[] arrayNumber;


    public NumberAndStringProcessor(String[] array) {
        arrayString = array;
    }


    public NumberAndStringProcessor(int[] array) {
        arrayNumber = array;
    }


    public void process(StringProcess process) {

        String[] newString = new String[arrayString.length];

        for (int i = 0; i < arrayString.length; i++) {
           newString[i] = process.process(arrayString[i]);
        }
        System.out.println("----------------------------");
        System.out.println("Your string after process is:" + Arrays.toString(newString));
        System.out.println("----------------------------");
    }


    public void process(NumberProcess process) {

        int[] newNumber = new int[arrayNumber.length];

        for (int i = 0; i < arrayNumber.length; i++) {
            newNumber[i] = process.process(arrayNumber[i]);
        }
        System.out.println("----------------------------");
        System.out.println("Your numbers after process is:" + Arrays.toString(newNumber));
        System.out.println("----------------------------");
    }
}
