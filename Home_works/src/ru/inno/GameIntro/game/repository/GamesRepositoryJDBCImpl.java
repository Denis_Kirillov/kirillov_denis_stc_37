package ru.inno.GameIntro.game.repository;

import ru.inno.GameIntro.game.models.Game;
import ru.inno.GameIntro.game.models.Player;
import ru.inno.GameIntro.game.utils.CustomDataSource;

import javax.sql.DataSource;
import java.sql.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static ru.inno.GameIntro.game.utils.JdbcUtil.closeJdbcObjects;

public class GamesRepositoryJDBCImpl implements GamesRepository {
    private static final String JDBC_URL = "jdbc:postgresql://localhost:5432/stc_37";
    private static final String JDBC_USER = "postgres";
    private static final String JDBC_PASSWORD = "Java_stc_37";
    //language=SQL
    private static final String SQL_INSERT_GAME = "insert into game(time,first_player, second_player," +
            " first_player_shot_count, second_player_shot_count, amount_of_seconds) values (?,?,?,?,?,?)";

    //language=SQL
    private static final String SQL_FIND_GAME_BY_ID = "select * from (select *, g.id as game_id, f.id as f_id," +
            " f.ip as f_ip, f.name as f_name, f.points as f_points, f.win_count as f_wc, f.lose_count as f_lc," +
            " s.id as s_id, s.ip as s_ip, s.name as s_name, s.points as s_points, s.win_count as s_wc," +
            " s.lose_count as s_lc from game g " +
            "left join player f on g.first_player = f.id " +
            "left join player s on g.second_player = s.id) s " +
            "where game_id = ?";

    //language=SQL
    private static final String SQL_UPDATE_GAME_BY_ID = "update game set time = ?, first_player = ?, second_player = ?," +
            "first_player_shot_count = ?, second_player_shot_count = ?, amount_of_seconds = ? where id = ?";

    static DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;

    private static final RowMapper<Game> gameRowMapper = row -> new Game (
            LocalDateTime.parse(row.getString("time"),formatter),
            findPlayerByNick(row.getString("f_name")),
            findPlayerByNick(row.getString("s_name")),
            row.getInt("first_player_shot_count"),
            row.getInt("second_player_shot_count"),
            row.getLong("amount_of_seconds"));

    DataSource dataSource;

    public GamesRepositoryJDBCImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void save(Game game) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet generatedId = null;

        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_INSERT_GAME, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, game.getDateTime().format(formatter));
            statement.setLong(2, game.getPlayerFirst().getId());
            statement.setLong(3, game.getPlayerSecond().getId());
            statement.setInt(4, game.getPlayerFirstShotsCount());
            statement.setInt(5, game.getPlayerSecondShotsCount());
            statement.setLong(6, game.getSecondsGameTimeAmount());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {

                throw new SQLException("Can't insert data");
            }

            generatedId = statement.getGeneratedKeys();

            if (generatedId.next()) {

                game.setId(generatedId.getLong("id"));
            } else {

                throw new SQLException("Can't retrieve id");
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, generatedId);
        }
    }

    @Override
    public Game findById(Long gameId) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rows = null;
        Game game = null;

        try {
            connection = dataSource.getConnection();

            statement = connection.prepareStatement(SQL_FIND_GAME_BY_ID);
            statement.setLong(1, gameId);

            rows = statement.executeQuery();

            if (rows.next()) {
                game = gameRowMapper.mapRow(rows);
                game.setId(rows.getLong("game_id"));
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, rows);
        }
        return game;
    }

    @Override
    public void update(Game game) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet generatedId = null;

        try {
            connection = dataSource.getConnection();

            statement = connection.prepareStatement(SQL_UPDATE_GAME_BY_ID);
            statement.setString(1, game.getDateTime().format(formatter));
            statement.setLong(2, game.getPlayerFirst().getId());
            statement.setLong(3, game.getPlayerSecond().getId());
            statement.setInt(4, game.getPlayerFirstShotsCount());
            statement.setInt(5, game.getPlayerSecondShotsCount());
            statement.setLong(6, game.getSecondsGameTimeAmount());
            statement.setLong(7, game.getId());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {

                throw new SQLException("Can't insert data");
            }


        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, generatedId);
        }
    }

    private static Player findPlayerByNick(String nickname){
        DataSource dataSource = new CustomDataSource(JDBC_URL,JDBC_USER,JDBC_PASSWORD);
        PlayersRepositoryJDBCImpl playersRepository = new PlayersRepositoryJDBCImpl(dataSource);
        return playersRepository.findByNickname(nickname);
    }


}
