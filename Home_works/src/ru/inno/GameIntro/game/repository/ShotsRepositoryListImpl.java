package ru.inno.GameIntro.game.repository;

import ru.inno.GameIntro.game.models.Shot;

import java.util.ArrayList;
import java.util.List;

/**
 * 25.03.2021
 * GameIntro
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class ShotsRepositoryListImpl implements ShotsRepository {

    private List<Shot> shots;

    public ShotsRepositoryListImpl() {
        this.shots = new ArrayList<>();
    }

    @Override
    public void save(Shot shot) {
        this.shots.add(shot);
    }
}
