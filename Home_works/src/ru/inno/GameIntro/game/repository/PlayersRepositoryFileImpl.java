package ru.inno.GameIntro.game.repository;

import ru.inno.GameIntro.game.models.Player;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class PlayersRepositoryFileImpl implements PlayersRepository {

    private String dbFileName;
    private String sequenceFileName;

    public PlayersRepositoryFileImpl(String dbFileName, String sequenceFileName) {
        this.dbFileName = dbFileName;
        this.sequenceFileName = sequenceFileName;
    }

    @Override
    public Player findByNickname(String nickname) {
        String line;
        String[] parsedLines = new String[6];
        try {
            BufferedReader reader = new BufferedReader(new FileReader(dbFileName));

            while ((line = reader.readLine()) != null) {
                if (!line.isEmpty()) {
                    parsedLines = line.split("#");

                    if (nickname.equals(parsedLines[1])) {
                        Player player = new Player(parsedLines[2], parsedLines[1], Integer.parseInt(parsedLines[3])
                                , Integer.parseInt(parsedLines[4]), Integer.parseInt(parsedLines[5]));
                        player.setId(Long.parseLong(parsedLines[0]));
                        reader.close();
                        return player;

                    }
                }
            }

            reader.close();
            return null;

        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

    }

    @Override
    public void save(Player player) {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(dbFileName, true));
            player.setId(generateID());
            writer.write(player.getId() + "#" + player.getName() + "#" + player.getIp() + "#" +
                    player.getPoints() + "#" + player.getMaxWinsCount() + "#" + player.getMaxLosesCount() + "\n");
            writer.close();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void update(Player player) {
        Map<Long, Player> players = new HashMap<>();
        String line;
        String[] parsedLines;
        Player parsedPlayer;
        try {
            BufferedReader reader = new BufferedReader(new FileReader(dbFileName));

            while ((line = reader.readLine()) != null) {
                if (!line.isEmpty()) {
                    parsedLines = line.split("#");
                    parsedPlayer = new Player(parsedLines[2], parsedLines[1], Integer.parseInt(parsedLines[3])
                            , Integer.parseInt(parsedLines[4]), Integer.parseInt(parsedLines[5]));
                    parsedPlayer.setId(Long.parseLong(parsedLines[0]));
                    players.put(Long.parseLong(parsedLines[0]), parsedPlayer);
                }
            }
            reader.close();

            if (players.containsKey(player.getId())) {
                players.put(player.getId(), player);
            } else {
                System.err.println("Нельзя обновить несуществующего игрока");
            }

            BufferedWriter writer = new BufferedWriter(new FileWriter(dbFileName));
            for (Map.Entry<Long,Player> entry : players.entrySet()) {
                writer.write(entry.getValue().getId() + "#" + entry.getValue().getName() + "#" +
                        entry.getValue().getIp() + "#" + entry.getValue().getPoints() + "#" +
                        entry.getValue().getMaxWinsCount() + "#" + entry.getValue().getMaxLosesCount() + "\n");
            }
            writer.close();

        } catch (IOException e) {
            throw new IllegalStateException(e);
        }


    }

    public Long generateID() {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(sequenceFileName));
            String lastId = reader.readLine();
            long id = Long.parseLong(lastId);
            reader.close();

            BufferedWriter writer = new BufferedWriter(new FileWriter(sequenceFileName));
            writer.write(String.valueOf(id + 1));
            writer.close();

            return id;
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
