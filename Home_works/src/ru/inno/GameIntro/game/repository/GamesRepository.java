package ru.inno.GameIntro.game.repository;

import ru.inno.GameIntro.game.models.Game;

/**
 * 25.03.2021
 * GameIntro
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface GamesRepository {
    void save(Game game);

    Game findById(Long gameId);

    void update(Game game);
}
