package ru.inno.GameIntro.game.repository;


import ru.inno.GameIntro.game.models.Player;
import javax.sql.DataSource;
import java.sql.*;
import static ru.inno.GameIntro.game.utils.JdbcUtil.closeJdbcObjects;


public class PlayersRepositoryJDBCImpl implements PlayersRepository {

    //language=SQL
    private static final String SQL_INSERT_PLAYER = "insert into player(ip,name, points," +
            " win_count, lose_count) values (?,?,?,?,?)";

    //language=SQL
    private static final String SQL_FIND_PLAYER_BY_NICKNAME = "select player.id as player_id, * from player where name = ?";

    //language=SQL
    private static final String SQL_UPDATE_PLAYER_BY_NICKNAME = "update player set ip = ?, points = ?," +
            "win_count = ?, lose_count = ? where name = ?";

    private static final RowMapper<Player> playerRowMapper = row -> new Player (
            row.getString("ip"),
            row.getString("name"),
            row.getInt("points"),
            row.getInt("win_count"),
            row.getInt("lose_count"));

    DataSource dataSource;

    public PlayersRepositoryJDBCImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Player findByNickname(String nickname) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rows = null;
        Player player = null;

        try {
            connection = dataSource.getConnection();

            statement = connection.prepareStatement(SQL_FIND_PLAYER_BY_NICKNAME);
            statement.setString(1, nickname);

            rows = statement.executeQuery();

            if (rows.next()) {
                player = playerRowMapper.mapRow(rows);
                player.setId(rows.getLong("id"));
            }


        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, rows);
        }
        return player;
    }

    @Override
    public void save(Player player) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet generatedId = null;

        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_INSERT_PLAYER, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, player.getIp());
            statement.setString(2, player.getName());
            statement.setLong(3, player.getPoints());
            statement.setInt(4, player.getMaxWinsCount());
            statement.setInt(5, player.getMaxLosesCount());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {

                throw new SQLException("Can't insert data");
            }

            generatedId = statement.getGeneratedKeys();

            if (generatedId.next()) {

                player.setId(generatedId.getLong("id"));
            } else {

                throw new SQLException("Can't retrieve id");
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, generatedId);
        }
    }

    @Override
    public void update(Player player) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet generatedId = null;

        try {
            connection = dataSource.getConnection();

            statement = connection.prepareStatement(SQL_UPDATE_PLAYER_BY_NICKNAME);
            statement.setString(1, player.getIp());
            statement.setLong(2, player.getPoints());
            statement.setInt(3, player.getMaxWinsCount());
            statement.setInt(4, player.getMaxLosesCount());
            statement.setString(5, player.getName());


            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {

                throw new SQLException("Can't insert data");
            }


        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(connection, statement, generatedId);
        }
    }
}
