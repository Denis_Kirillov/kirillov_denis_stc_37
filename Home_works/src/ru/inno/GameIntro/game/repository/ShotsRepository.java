package ru.inno.GameIntro.game.repository;

import ru.inno.GameIntro.game.models.Shot;

/**
 * 25.03.2021
 * GameIntro
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface ShotsRepository {
    void save(Shot shot);
}
