package ru.inno.GameIntro.game.services;

import ru.inno.GameIntro.game.dto.StatisticDto;
import ru.inno.GameIntro.game.models.Game;
import ru.inno.GameIntro.game.models.Player;
import ru.inno.GameIntro.game.models.Shot;
import ru.inno.GameIntro.game.repository.GamesRepository;
import ru.inno.GameIntro.game.repository.PlayersRepository;
import ru.inno.GameIntro.game.repository.ShotsRepository;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;

/**
 * 25.03.2021
 * GameIntro
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// бизнес-логика
public class GameServiceImpl implements GameService {

    private PlayersRepository playersRepository;

    private GamesRepository gamesRepository;

    private ShotsRepository shotsRepository;

    public GameServiceImpl(PlayersRepository playersRepository, GamesRepository gamesRepository, ShotsRepository shotsRepository) {
        this.playersRepository = playersRepository;
        this.gamesRepository = gamesRepository;
        this.shotsRepository = shotsRepository;
    }

    @Override
    public Long startGame(String firstIp, String secondIp, String firstPlayerNickname, String secondPlayerNickname) {
        // получили информацию об обоих игроках
        Player first = checkIfExists(firstIp, firstPlayerNickname);
        Player second = checkIfExists(secondIp, secondPlayerNickname);
        // создали игру
        Game game = new Game(LocalDateTime.now(), first, second, 0, 0, 0L);
        // сохранили игру в репозитории
        gamesRepository.save(game);
        return game.getId();
    }

    private Player checkIfExists(String ip, String nickname) {
        Player player = playersRepository.findByNickname(nickname);
        // если нет первого игрока под таким именем
        if (player == null) {
            // создаем игрока
            player = new Player(ip, nickname, 0, 0, 0);
            // сохраняем его в репозитории
            playersRepository.save(player);
        } else {
            // если такой игрок был -> обновляем у него IP-адрес
            player.setIp(ip);
            playersRepository.update(player);
        }

        return player;
    }

    @Override
    public void shot(Long gameId, String shooterNickname, String targetNickname) {
        // получаем того, кто стрелял из репозитория
        Player shooter = playersRepository.findByNickname(shooterNickname);
        // получаем того, в кого стреляли из репозитория
        Player target = playersRepository.findByNickname(targetNickname);
        // получаем игру
        Game game = gamesRepository.findById(gameId);
        // создаем выстрел
        Shot shot = new Shot(LocalDateTime.now(), game, shooter, target);
        // увеличиваем очки у стреляющего
        shooter.setPoints(shooter.getPoints() + 1);
        // если стрелявший - первый игрок
        if (game.getPlayerFirst().getName().equals(shooterNickname)) {
            // сохраняем информацию о выстреле в игре
            game.setPlayerFirstShotsCount(game.getPlayerFirstShotsCount() + 1);
        }
        // если стрелявший - второй игрок
        if (game.getPlayerSecond().getName().equals(shooterNickname)) {
            // сохраняем информацию о выстреле в игре
            game.setPlayerSecondShotsCount(game.getPlayerSecondShotsCount() + 1);
        }
        // обновляем данные по стреляющему
        playersRepository.update(shooter);
        // обновляем данные по игре
        gamesRepository.update(game);
        // сохраняем выстрел
        shotsRepository.save(shot);
    }

    @Override
    public StatisticDto finishGame(Long gameId) {

        Game game = gamesRepository.findById(gameId);
        String winner = "Ничья";

        game.setSecondsGameTimeAmount(countAmountOfSeconds(game.getDateTime()));

        Player first = playersRepository.findByNickname(game.getPlayerFirst().getName());

        Player second = playersRepository.findByNickname(game.getPlayerSecond().getName());

        if (game.getPlayerFirstShotsCount() > game.getPlayerSecondShotsCount()) {                   // Определение победителя

            winner = first.getName();
            first.setMaxWinsCount(first.getMaxWinsCount() + 1);
            second.setMaxLosesCount(second.getMaxLosesCount() + 1);

        } else if (game.getPlayerFirstShotsCount() < game.getPlayerSecondShotsCount()) {

            winner = second.getName();
            second.setMaxWinsCount(second.getMaxWinsCount() + 1);
            first.setMaxLosesCount(first.getMaxLosesCount() + 1);

        }

        playersRepository.update(first);
        playersRepository.update(second);
        gamesRepository.update(game);

        return new StatisticDto(first.getName(), second.getName(), game.getPlayerFirstShotsCount(),
                game.getPlayerSecondShotsCount(), winner, gameId, game.getSecondsGameTimeAmount(),
                playersRepository);
    }

    private Long countAmountOfSeconds(LocalDateTime time){
        LocalDateTime currentTime = LocalDateTime.now();
        return currentTime.toEpochSecond(ZoneOffset.UTC) - time.toEpochSecond(ZoneOffset.UTC);      // количество секунд
    }


}
