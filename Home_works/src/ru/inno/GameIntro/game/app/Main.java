package ru.inno.GameIntro.game.app;

import ru.inno.GameIntro.game.dto.StatisticDto;
import ru.inno.GameIntro.game.repository.*;
import ru.inno.GameIntro.game.services.GameService;
import ru.inno.GameIntro.game.services.GameServiceImpl;
import ru.inno.GameIntro.game.utils.CustomDataSource;

import javax.sql.DataSource;
import java.util.Random;
import java.util.Scanner;

public class Main {
    private static final String JDBC_URL = "jdbc:postgresql://localhost:5432/stc_37";
    private static final String JDBC_USER = "postgres";
    private static final String JDBC_PASSWORD = "Java_stc_37";

    public static void main(String[] args) {
        DataSource dataSource = new CustomDataSource(JDBC_URL, JDBC_USER, JDBC_PASSWORD);

        PlayersRepository playersRepository = new PlayersRepositoryJDBCImpl(dataSource);
        GamesRepository gamesRepository = new GamesRepositoryJDBCImpl(dataSource);
        ShotsRepository shotsRepository = new ShotsRepositoryJDBCImpl(dataSource);
        GameService gameService = new GameServiceImpl(playersRepository, gamesRepository, shotsRepository);

        Scanner scanner = new Scanner(System.in);
        Random random = new Random();

        for (int j = 1; j < 4; j++) {
            System.out.println("Игра № " + j);
            System.out.println("введите ник первого игрока");
            String first = scanner.nextLine();
            System.out.println("введите ник второго игрока");
            String second = scanner.nextLine();

            Long gameId = gameService.startGame("127.0.0.1", "127.0.0.2", first, second);
            String shooter = first;
            String target = second;
            int i = 0;
            while (i < 10) {

                System.out.println(shooter + " делайте выстрел в " + target);
                scanner.nextLine();

                int success = random.nextInt(2);

                if (success == 0) {
                    System.out.println("Успешно!");
                    gameService.shot(gameId, shooter, target);
                } else {
                    System.out.println("Промах!");
                }

                String temp = shooter;
                shooter = target;
                target = temp;
                i++;
            }

            StatisticDto statistic = gameService.finishGame(gameId);
            System.out.println(statistic);
        }
    }
}
