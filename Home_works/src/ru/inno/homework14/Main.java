package ru.inno.homework14;

import ru.inno.homework14.hashmap.HashMapImpl;

public class Main {

    public static void main(String[] args) {
	    Map<String, String> map = new HashMapImpl<>();

	    map.put("Марсель", "Сидиков");
	    map.put("Виктор", "Евлампьев");
	    map.put("Айрат", "Мухутдинов");
	    map.put("Даниил", "Вдовинов");
	    map.put("Даниил", "Богомолов");
	    map.put("Джамиль", "Садыков");
	    map.put("Николай", "Пономарев");


		System.out.println("Values of your map: ");
		System.out.println(map.get("Даниил"));
		System.out.println(map.get("Айрат"));
		System.out.println(map.get("Марсель"));
		System.out.println(map.get("Виктор"));
		System.out.println(map.get("Джамиль"));
		System.out.println(map.get("Николай"));
		System.out.println(map.get("Неправильный ключ"));

    }
}
