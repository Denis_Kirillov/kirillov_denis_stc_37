package ru.inno.homework6;

import java.util.Random;

public class Channel {

    private String name;
    private String[][] programNames;
    private Program program;
    Random random = new Random();


    public void setProgram(int index) {

        program.setName(programNames[index][random.nextInt(programNames[index].length)]);

    }


    public void setName(String name) {

        this.name = name;

    }


    public String getName() {

        return name;

    }


    public void showProgram() {

        System.out.print("You are looking: ");
        System.out.println(program.getName());

    }


    public Channel(Program program, String[][] programNames) {

        this.program = program;
        this.programNames = programNames;

    }

}
