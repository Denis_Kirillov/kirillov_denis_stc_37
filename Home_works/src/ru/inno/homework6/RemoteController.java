package ru.inno.homework6;

public class RemoteController {

    private int chosenChannel;
    private TV tv;

    public void setTV(TV tv) {

        this.tv = tv;
        this.tv.setRemote(this);

    }


    public void setChosenChannel(int chosenChannel) {

        this.chosenChannel = chosenChannel;

    }


    public int getChosenChannel() {

        return chosenChannel;

    }


    public void showChosenChannel(){

        tv.setChannel();
        tv.showChannel(chosenChannel);

    }

}
