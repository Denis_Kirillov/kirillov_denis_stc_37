package ru.inno.homework6;

import java.util.Scanner;

public class Program_6_1 {


    public static void main(String[] args) {

        String[][] programNames = new String[4][];
        String[] channelNames = new String[4];

        programNamesInit(programNames);
        channelNamesInit(channelNames);

        Program program = new Program();
        Channel channel = new Channel(program, programNames);
        TV tv = new TV(channel, channelNames);
        RemoteController remote = new RemoteController();

        remote.setTV(tv);
        lookingTV(remote);

    }



    public static void programNamesInit(String[][] programNames) {

        programNames[0] = new String[] {"News" , "Weather", "Talent show"};
        programNames[1] = new String[] {"Tom & Jerry", "Nu pogodi", "Disney club"};
        programNames[2] = new String[] {"Sport news", "Footbal match", "Hockey match"};
        programNames[3] = new String[] {"Music box live", "30 best clips", "Music news"};

    }


    public static void channelNamesInit(String[] channelNames) {

       channelNames[0] = "ru.inno.homework6.Channel 1";
       channelNames[1] = "Kid's channel";
       channelNames[2] = "Sport channel";
       channelNames[3] = "Music channel";

    }

    public static void helpMenu() {

        System.out.println("\n\nEnter channel number from 1 to 4. Enter 0 to exit");

    }


    public static void lookingTV(RemoteController remote) {

        Scanner scanner = new Scanner(System.in);
        helpMenu();
        int channelInput = scanner.nextInt();

        while (channelInput != 0) {

            if (channelInput > 0 & channelInput < 5) {

                remote.setChosenChannel(channelInput - 1);
                remote.showChosenChannel();
                helpMenu();

            } else {

                helpMenu();

            }
            channelInput = scanner.nextInt();

        }
    }
}
