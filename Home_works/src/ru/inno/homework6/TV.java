package ru.inno.homework6;

public class TV {

    private String[] channelNames;

    private Channel channel;
    private RemoteController remote;


    public void setRemote(RemoteController remote) {

        this.remote = remote;

    }


    public void setChannel() {

        channel.setName(channelNames[remote.getChosenChannel()]);

    }


    public void showChannel(int index) {

        System.out.println("You are on " + channel.getName());
        channel.setProgram(index);
        channel.showProgram();

    }


    public TV(Channel channel, String[] channelNames) {

        this.channel = channel;
        this.channelNames = channelNames;

    }

}
