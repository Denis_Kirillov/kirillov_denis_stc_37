package ru.inno.homework19;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CarsRepositoryFileImpl implements CarsRepository {


    private static final Function<String, Cars> carsMapper = line -> {
        String[] parsedLine = line.split("#");
        return new Cars(parsedLine[0], parsedLine[1], parsedLine[2],
                Long.parseLong(parsedLine[3]),Long.parseLong(parsedLine[4]));
    };

    private static final Function<Cars, String> getNumber = Cars::getNumber;
    private static final Function<Cars, String> getModel = Cars::getModel;



    @Override
    public List<String> findNumbers(String color) {

        try {

            BufferedReader reader = new BufferedReader(new FileReader("dbCars.txt"));
            List<String> numbers;
            numbers = reader
                    .lines()
                    .map(carsMapper)
                    .filter(car -> car.getColor().equals(color) || car.getMileage() == 0)
                    .map(getNumber)
                    .collect(Collectors.toList());
            reader.close();
            return numbers;
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException();
        } catch (IOException e) {
            throw new IllegalStateException();
        }
    }

    @Override
    public int findCountInPriceRange(long lowPrice, long highPrice) {

        try {

            BufferedReader reader = new BufferedReader(new FileReader("dbCars.txt"));
            List<String> models;
            models = reader
                    .lines()
                    .map(carsMapper)
                    .filter(car -> car.getCost() > lowPrice && car.getCost() < highPrice)
                    .map(getModel)
                    .distinct()
                    .collect(Collectors.toList());
            reader.close();
            return models.size();
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException();
        } catch (IOException e) {
            throw new IllegalStateException();
        }
    }

    @Override
    public String findColorOfCheapestCar() {
        try {

            BufferedReader reader = new BufferedReader(new FileReader("dbCars.txt"));
            Cars car;
            car = reader
                    .lines()
                    .map(carsMapper)
                    .min(Cars::compare)
                    .get();
            reader.close();
            return car.getColor();

        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException();
        } catch (IOException e) {
            throw new IllegalStateException();
        }

    }

    @Override
    public Double findAveragePriceOfModel(String model) {
        try {

            BufferedReader reader = new BufferedReader(new FileReader("dbCars.txt"));
            Double average;
            average = reader
                    .lines()
                    .map(carsMapper)
                    .filter(car -> car.getModel().equals(model))
                    .collect(Collectors.averagingLong(Cars::getCost));
            reader.close();
            return average;
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException();
        } catch (IOException e) {
            throw new IllegalStateException();
        }
    }
}
