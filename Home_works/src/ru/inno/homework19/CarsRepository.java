package ru.inno.homework19;

import java.util.List;

public interface CarsRepository {

        List<String> findNumbers(String color);

        int findCountInPriceRange(long lowPrice, long highPrice);

        String findColorOfCheapestCar();

        Double findAveragePriceOfModel(String model);

}
