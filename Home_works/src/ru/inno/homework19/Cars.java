package ru.inno.homework19;

import java.util.Objects;

public class Cars {
    private String number;
    private String model;
    private String color;
    private Long mileage;
    private Long cost;

    public Cars(String number, String model, String color, Long mileage, Long cost) {
        this.number = number;
        this.model = model;
        this.color = color;
        this.mileage = mileage;
        this.cost = cost;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getModel() {
        return model;
    }


    public String getColor() {
        return color;
    }


    public Long getMileage() {
        return mileage;
    }


    public Long getCost() {
        return cost;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cars cars = (Cars) o;
        return Objects.equals(number, cars.number) && Objects.equals(model, cars.model) && Objects.equals(color, cars.color) && Objects.equals(mileage, cars.mileage) && Objects.equals(cost, cars.cost);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, model, color, mileage, cost);
    }

    @Override
    public String toString() {
        return "Cars{" +
                "number='" + number + '\'' +
                ", model='" + model + '\'' +
                ", color='" + color + '\'' +
                ", mileage=" + mileage +
                ", cost=" + cost +
                '}';
    }

    public static int compare(Cars first, Cars second) {
        if (first.getCost() > second.getCost()) {
            return 1;
        }
        return -1;
    }
}
