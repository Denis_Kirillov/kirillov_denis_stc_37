package ru.inno.homework19;

import java.util.Scanner;

public class UserInterface {
    CarsRepository carsRepository;

    public UserInterface(CarsRepository carsRepository) {
        this.carsRepository = carsRepository;
    }

    public void actionChoice() {

        Scanner scanner = new Scanner(System.in);
        actionMenu();

        int choiceActionValue = scanner.nextInt();

        while (choiceActionValue != 0) {

            switch(choiceActionValue){

                case 1:

                    choiceActionValue = findNumbersWithColor();
                    break;

                case 2:

                    choiceActionValue = findModelsInRange();
                    break;

                case 3:

                    choiceActionValue = findLowestPriceColor();
                    break;

                case 4:

                    choiceActionValue = findAveragePrice();
                    break;

                default:
                    actionMenu();
                    choiceActionValue = scanner.nextInt();
                    break;
            }

        }
    }

    public void actionMenu() {

        System.out.println("\nChoose what to do : ");
        System.out.println("1 - Show car numbers with specific color or zero mileage");
        System.out.println("2 - Count unique cars in cost range");
        System.out.println("3 - Show color of cheapest car");
        System.out.println("4 - Show average cost of specific model");
        System.out.println("0 - Exit");

    }

    public void colorMenu() {

        System.out.println("\nEnter color from list: ");
        System.out.println("Black");
        System.out.println("Red");
        System.out.println("White");
        System.out.println("Blue");
        System.out.println("Green");

    }

    public void modelMenu() {

        System.out.println("\nEnter model from list: ");
        System.out.println("Camry");
        System.out.println("Focus");
        System.out.println("Lancer");
        System.out.println("Rio");
        System.out.println("Nexia");
        System.out.println("Megane");

    }


    public Integer findNumbersWithColor() {
        Scanner scanner = new Scanner(System.in);

        colorMenu();
        System.out.println("Car number: " + carsRepository.findNumbers(scanner.nextLine()));

        actionMenu();

        return scanner.nextInt();
    }

    public Integer findModelsInRange() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("\nEnter low price and high price: ");
        System.out.println("Car count is: " +
                carsRepository.findCountInPriceRange(scanner.nextLong(),scanner.nextLong()));

        actionMenu();

        return scanner.nextInt();
    }

    public Integer findLowestPriceColor() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("\nColor of the lowest price car is: ");
        System.out.println("Color is: " + carsRepository.findColorOfCheapestCar());

        actionMenu();
        return scanner.nextInt();
    }

    public Integer findAveragePrice() {
        Scanner scanner = new Scanner(System.in);
        modelMenu();
        System.out.println("Average price is: " +
                carsRepository.findAveragePriceOfModel(scanner.nextLine()));

        actionMenu();
        return scanner.nextInt();
    }

}
