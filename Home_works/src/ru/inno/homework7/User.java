package ru.inno.homework7;

public class User {


    private String firstName = "";
    private String lastName = "";
    private int age = 0;
    private boolean isWorker = false;


    public static Builder builder() {
        return new Builder();
    }


    public String getFirstName() {
        return firstName;
    }


    public String getLastName() {
        return lastName;
    }


    public int getAge() {
        return age;
    }


    public boolean isWorker() {
        return isWorker;
    }


    static class Builder {

        private String firstNameValue = "";
        private String lastNameValue = "";
        private int ageValue = 0;
        private boolean isWorkerValue = false;


        public Builder firstname(String firstName) {
            this.firstNameValue = firstName;
            return this;
        }


        public Builder lastName(String lastName) {
            this.lastNameValue = lastName;
            return this;
        }


        public Builder age(int age) {
            this.ageValue = age;
            return this;
        }


        public Builder isWorker(boolean isWorker) {
            this.isWorkerValue = isWorker;
            return this;
        }


        public User build() {

            User user = new User();
            user.firstName = this.firstNameValue;
            user.lastName = this.lastNameValue;
            user.age = this.ageValue;
            user.isWorker = this.isWorkerValue;

            return user;

        }
    }










}
