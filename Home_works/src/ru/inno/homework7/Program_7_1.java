package ru.inno.homework7;

public class Program_7_1 {

    public static void main(String[] args) {

        User user = User.builder().firstname("Denis").lastName("Kirillov").age(29).isWorker(true).build();
        System.out.println(user.getFirstName() + " " + user.getLastName() + " " + user.getAge() + " " + user.isWorker());

    }
}
