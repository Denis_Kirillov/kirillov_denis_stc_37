package ru.inno.homework2.task4;

import java.util.Scanner;
import java.util.Arrays;

class Program_2_4 {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter size of array: ");
		int n = scanner.nextInt();
		int array[] = new int[n];
		int minIndex = 0;									// индекс минимального числа в массиве
		int maxIndex = 0;									// индекс максимального числа в массиве
		

		for (int i = 0; i < array.length; i++) {		

			array[i] = scanner.nextInt();

		}


		int min = array[0];
		int max = array[0];

		for (int i = 0; i < array.length; i++) {

			if (array[i] > max) {							// поиск и присваивание максимального числа в массиве и его индекса
				max = array[i];
				maxIndex = i;
			}

			if (array[i] < min) {							// поиск и присваивание минимального числа в массиве и его индекса
				min = array[i];
				minIndex = i;
			}
		}


		array[maxIndex] = min;								// перестановка максимального и минимальго чисел местами
		array[minIndex] = max;
		

		System.out.println(Arrays.toString(array)); 
	}

}