package ru.inno.homework2.task2;

import java.util.Scanner;
import java.util.Arrays;

class Program_2_2 {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter size of array: ");
		int n = scanner.nextInt();
		int array[] = new int[n];
		int arrayInv[] = new int[n];						// инвертированный массив
		
		for (int i = 0; i < arrayInv.length; i++) {

			arrayInv[i] = scanner.nextInt();			
			
		}

		for (int i = 0; i < arrayInv.length; i++) {

			array[i] = arrayInv[arrayInv.length - 1 - i];	// инверсия выводного массива
			
		}
		

		System.out.println(Arrays.toString(array)); 
	}

}