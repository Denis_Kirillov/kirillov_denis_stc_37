package ru.inno.homework2.task5;

import java.util.Scanner;
import java.util.Arrays;

class Program_2_5 {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter size of array: ");
		int n = scanner.nextInt();
		int array[] = new int[n];
		int arrayBuf = 0;										// буферная переменная


		for (int i = 0; i < array.length; i++) {

			array[i] = scanner.nextInt();

		}


		for (int i = 1; i < array.length; i++) {				// Общий цикл n-1 проходов

			for (int j = 0; j < array.length - i; j++) {		// проход слева направо

				if (array[j] > array[j+1]) {

					arrayBuf = array[j+1];						// Смена местами соседних значений
					array[j+1] = array[j];
					array[j] = arrayBuf;
				}


			}

		}

		System.out.println(Arrays.toString(array)); 
	}

}