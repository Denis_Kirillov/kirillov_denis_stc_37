package ru.inno.homework2.task6;

class Program_2_6 {

	public static void main(String[] args) {
		
		int array[] = {4,2,3,56,7};
		int number = 0;
		int power = 0;														// Степень для определения разряда элементов массива в number
		int arrayBuf = 0;													// буферная переменная

		for (int i = array.length-1; i >= 0; i--) {							// проход по все элементам с конца

			arrayBuf = array[i];			
			number = number + (arrayBuf * (int) Math.pow(10,power));		// сложение предыдущих элементов массива с текущим с учетом его положения в number  

			while (arrayBuf / 10 > 0) {										// цикл на случай, если элемент массива состоит из 2х и более цифр,
																			// необходимый для смещения след числа влево на правильное количество символов
				power++;													
				arrayBuf = arrayBuf / 10;

			}

			power++;

		}

		
		System.out.println(number); 
	}

}