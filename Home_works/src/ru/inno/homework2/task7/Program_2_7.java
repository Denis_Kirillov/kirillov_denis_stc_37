package ru.inno.homework2.task7;

import java.util.Scanner;
import java.util.Arrays;

class Program_2_7 {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter size of array M: ");
		int m = scanner.nextInt();
		System.out.println("Enter size of array N: ");
		int n = scanner.nextInt();
		System.out.println("Enter 1 for clockwise, and 0 for anticlockwise ");
		int spin = scanner.nextInt();											// переменная для выбора направления вращения

		int array[][] = new int[m][n];
		int arrayValue = 0;														// Значение, которое будет заполнять массив
		int xBuf = 0;															// буферное значение для строк массива
		int yBuf = 0;															// буферное значение для столбцов массива
		int bound = 0;															// буферное значение для границы условия
		int min = 0;															

		if (m <= n) {														
			min = m;
		} else {
			min = n;
		}

		for (int cycle = 0; cycle < (min + 1) / 2; cycle++) {					// Цикл  одного витка, количество витков в массиве = (min + 1) / 2 

			/////////////////////////											// 1 проход
			
			if (spin == 1) {													// смена границ обхода для разных направлений вращения

				bound = n - cycle;

			} else {
					
				bound = m - cycle;

			}
				
			for (int i = cycle; i < bound; i++) {								// первый проход витка 

				if (spin == 1) {												// Назначение столбцов и строк 

					xBuf = cycle;
					yBuf = i;
					
				} else {

					xBuf = i;
					yBuf = cycle;
					
				}

				array[xBuf][yBuf] = ++arrayValue;									// заполнение текущей позиции

			}

			if (arrayValue == (m * n)) {											// остановка цикла витка при заполнении всех доступных позиций
				break;
			}

			/////////////////////////////											// 2 проход

			if (spin == 1) {

				bound = m - cycle;

			} else {
					
				bound = n - cycle;

			}
				
			for (int j = cycle + 1; j < bound; j++) {

				if (spin == 1) {

					xBuf = j;
					yBuf = n - cycle - 1;
					
				} else {

					xBuf = m - cycle - 1;
					yBuf = j;
					
				}
								
				array[xBuf][yBuf] = ++arrayValue;

			}

			if (arrayValue == (m * n)) {
				break;
			}

			//////////////////////////////											 // 3 проход

			if (spin == 1) {
				
				bound = n - cycle -2;

			} else {

				bound = m - cycle -2;

			}
				
			for (int i = bound; i >= cycle; i--) {

				if (spin == 1) {

					xBuf = m - cycle - 1;
					yBuf = i;

				} else {

					xBuf = i;
					yBuf = n - cycle - 1;

				}

				array[xBuf][yBuf] = ++arrayValue;

			}

			if (arrayValue == (m * n)) {
				break;
			}

			/////////////////////////////////										 // 4 проход

			if (spin == 1) {
				
				bound = m - cycle - 2;

			} else {
					
				bound = n - cycle - 2;

			}
				
			for (int j = bound; j >= cycle + 1; j--) {

				if (spin == 1) {

					xBuf = j;
					yBuf = cycle;
					
				} else {

					xBuf = cycle;
					yBuf = j;
					
				}
				
				array[xBuf][yBuf] = ++arrayValue;

			}

			if (arrayValue == (m * n)) {
				break;
			}

			//////////////////////////////////
		}


		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				System.out.print(array[i][j] + "\t");
			}
			System.out.println();
		}
	}

}