package ru.inno.homework8.classes;

public class Square extends ShapesWithCorners {

    public Square(double a) {
        super(a);
    }

    @Override
    public double getPerimetr() {
        return a * 4;
    }

    @Override
    public double getArea() {
        return a * a;
    }

    @Override
    public void scale(double factor) {
        this.a *= factor;
        System.out.println("Shape was scaled with factor = : " + factor);
    }

    @Override
    public void move(double addX , double addY) {
        super.move(addX, addY);
    }
}
