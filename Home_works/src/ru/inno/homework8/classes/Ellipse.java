package ru.inno.homework8.classes;

public class Ellipse extends ShapesWithoutCorners {

    private double radius2;

    public Ellipse(double radius, double radius2) {

        super(radius);
        this.radius2 = radius2;
    }

    @Override
    public double getPerimetr() {

        return 4 * (PI * radius * radius2 + Math.pow((radius - radius2),2)) / (radius + radius2);
    }

    @Override
    public double getArea() {

        return radius * radius2 * PI;
    }

    @Override
    public void scale(double factor) {

        this.radius *= factor;
        this.radius2 *= factor;
        System.out.println("Shape was scaled with factor = : " + factor);
    }

    @Override
    public void move(double addX , double addY) {

        super.move(addX, addY);
    }


}
