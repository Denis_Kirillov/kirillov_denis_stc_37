package ru.inno.homework8.classes;

public class Circle extends ShapesWithoutCorners {

    public Circle(double radius) {
        super(radius);
    }

    @Override
    public double getPerimetr() {
        return radius * 2 * PI;
    }

    @Override
    public double getArea() {
        return radius * radius * PI;
    }

    @Override
    public void scale(double factor) {
        this.radius *= factor;
        System.out.println("Shape was scaled with factor = : " + factor);
    }

    @Override
    public void move(double addX , double addY) {
        super.move(addX, addY);
    }
}
