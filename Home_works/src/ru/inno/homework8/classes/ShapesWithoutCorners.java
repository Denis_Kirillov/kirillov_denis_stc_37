package ru.inno.homework8.classes;

public abstract class ShapesWithoutCorners extends Shapes {

    protected double radius;
    public final static double PI = Math.PI;

        public ShapesWithoutCorners(double radius) {
            this.radius = radius;
        }
}
