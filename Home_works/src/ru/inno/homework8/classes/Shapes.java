package ru.inno.homework8.classes;

import ru.inno.homework8.interfaces.MovingInterface;
import ru.inno.homework8.interfaces.ScalingInterface;

public abstract class Shapes implements MovingInterface, ScalingInterface {

    protected double originX = 0.0;
    protected double originY = 0.0;

    public abstract double getPerimetr();

    public abstract double getArea();

    public void move(double addX , double addY) {
        this.originX = addX;
        this.originY = addY;
        System.out.println("Center of shape was moved to:  X = " + originX + " Y = " + originY);
    }

}
