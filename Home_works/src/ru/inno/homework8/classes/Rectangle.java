package ru.inno.homework8.classes;

public class Rectangle extends ShapesWithCorners {

    private double b;

    public Rectangle(double a, double b) {
        super(a);
        this.b = b;
    }

    @Override
    public double getPerimetr() {
        return (a + b) * 2;

    }

    @Override
    public double getArea() {
        return a * b;
    }

    @Override
    public void scale(double factor) {
        this.a *= factor;
        this.b *= factor;
        System.out.println("Shape was scaled with factor = : " + factor);
    }

    @Override
    public void move(double addX , double addY) {
        super.move(addX, addY);
    }

}
