package ru.inno.homework8;

import ru.inno.homework8.classes.Ellipse;
import ru.inno.homework8.classes.Square;
import ru.inno.homework8.classes.Rectangle;
import ru.inno.homework8.classes.Circle;
import ru.inno.homework8.classes.Shapes;

import java.util.Scanner;


public class Program_8_1 {


    public static void main(String[] args) {

        shapeActions(shapeChoosing());

    }


    public static void helpMenu() {

        System.out.println("Choose shape from the list :");
        System.out.println("1 - Square");
        System.out.println("2 - Rectangle");
        System.out.println("3 - Circle");
        System.out.println("4 - Ellipse");

    }


    public static void actionMenu() {

        System.out.println("\nChoose action for shape from the list :");
        System.out.println("1 - Count perimeter ");
        System.out.println("2 - Count area");
        System.out.println("3 - Scale shape");
        System.out.println("4 - Move origin of shape");
        System.out.println("5 - Enter new shape");
        System.out.println("0 - Exit");

    }


    public static Shapes shapeChoosing() {
        Scanner scanner = new Scanner(System.in);
        double a;
        double b;
        double radius;
        double radius2;
        
        Shapes shape = null;

        helpMenu();

        int shapeChoice = scanner.nextInt();

        if (shapeChoice > 0 & shapeChoice < 5) {

            switch (shapeChoice) {

                case (1):
                    System.out.println("Your choice is 'Square', enter size of a : ");
                    a = scanner.nextDouble();
                    shape = new Square(a);
                    break;

                case (2):
                    System.out.println("Your choice is 'Rectangle', enter size of a and b: ");
                    a = scanner.nextDouble();
                    b = scanner.nextDouble();
                    shape = new Rectangle(a, b);
                    break;

                case (3):
                    System.out.println("Your choice is 'Circle', enter size of radius: ");
                    radius = scanner.nextDouble();
                    shape = new Circle(radius);
                    break;

                case (4):
                    System.out.println("Your choice is 'Ellipse', enter size of radius1 and radius2: ");
                    radius = scanner.nextDouble();
                    radius2 = scanner.nextDouble();
                    shape = new Ellipse(radius, radius2);
                    break;

            }

        }
        return shape;

    }


    public static void shapeActions(Shapes shape) {

        Scanner scanner = new Scanner(System.in);
        double addX;
        double addY;
        double factor;

        if (shape != null) {

            actionMenu();
            int actionChoice = scanner.nextInt();

                while (actionChoice != 0) {

                    switch (actionChoice) {

                        case (1):
                            System.out.println("The perimeter of your shape is: " + shape.getPerimetr());
                            actionMenu();
                            actionChoice = scanner.nextInt();
                            break;

                        case (2):
                            System.out.println("The area of your shape is: " + shape.getArea());
                            actionMenu();
                            actionChoice = scanner.nextInt();
                            break;

                        case (3):
                            System.out.println("Enter factor for scaling: ");
                            factor = scanner.nextDouble();
                            shape.scale(factor);
                            actionMenu();
                            actionChoice = scanner.nextInt();

                            break;

                        case (4):
                            System.out.println("Enter new X and Y for shape's origin: ");
                            addX = scanner.nextDouble();
                            addY = scanner.nextDouble();
                            shape.move(addX, addY);
                            actionMenu();
                            actionChoice = scanner.nextInt();
                            break;

                        case (5):
                            shape = shapeChoosing();

                            if (shape != null) {
                                actionMenu();
                                actionChoice = scanner.nextInt();
                            } else {
                                actionChoice = 0;
                            }
                            break;

                        default:
                            actionMenu();
                            actionChoice = scanner.nextInt();
                            break;
                    }

                }

        } else {
            System.out.println("Wrong input. Closing program");
        }
    }

}
