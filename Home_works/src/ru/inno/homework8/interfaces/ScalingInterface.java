package ru.inno.homework8.interfaces;

public interface ScalingInterface {

    void scale(double factor);

}
