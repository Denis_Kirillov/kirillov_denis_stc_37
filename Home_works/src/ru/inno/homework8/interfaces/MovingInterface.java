package ru.inno.homework8.interfaces;

public interface MovingInterface {

    void move(double addX, double addY);

}
