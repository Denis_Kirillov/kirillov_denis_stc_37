package ru.inno.homework1.task3;

import java.util.Scanner;

class Program_1_3 {
	public static void main(String[] args) {

		System.out.println("Enter numbers, 0 means exit");
		Scanner scanner = new Scanner(System.in);

		long numberProd = 0L;																		  // Произведение чисел
		int digitsSum = 0;																			  // Сумма цифр
		int numberBuf = 0;																			  // Клон number для подсчета суммы цифр
		int checkValue = 0;																			  // Переменная для проверки количества делителей
		int firstProd = 1;																			  // Переменная для проверки первого перемножения
		int number = scanner.nextInt();																  // Вводимое число
		

		while (number != 0) {

			numberBuf = number;																		 
			digitsSum = 0;	
			checkValue = 0;

			while (numberBuf != 0) {																  // Подсчет суммы цифр

				digitsSum += numberBuf % 10;
				numberBuf /= 10;

			}

			for (int i = 2; i < digitsSum; i++) {													  // Определение простоты числа

				if (digitsSum % i == 0) { 

				checkValue++;																		  // нахождение количества делителей
					
				}		  
														  	  
			}

			if (checkValue < 1) {

				if (firstProd == 1) {															 	  // Проверка на первое перемножение

					numberProd = number;														 	  // Перемножение если сумма = простое
					firstProd = 0;																 	  // Отмена первого умножения
						
				} else {

					numberProd *= number;											 	  			  // Перемножение если сумма = простое
						
				}
			}		
			
			number = scanner.nextInt();				
		}

		System.out.println("product of simple numbers = " + numberProd);
	}
}