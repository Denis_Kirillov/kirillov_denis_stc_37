package ru.inno.homework1.task2;

//10000 = 0 0010 0111 0001 0000			14 делений мин
//12345 = 0 0011 0000 0011 1001 
//99999 = 1 1000 0110 1001 1111			17 делений мах


class Program_1_2 {
	public static void main(String[] args) {

		int number = 12345;
		long binaryNumber = 0L;
		int power = 0;													// степень
								

		binaryNumber += (number % 2) * ((long) Math.pow(10, power++));	// 1 деление
		number /= 2;
		binaryNumber += (number % 2) * ((long) Math.pow(10, power++));	// 2 деление
		number /= 2;
		binaryNumber += (number % 2) * ((long) Math.pow(10, power++));	// 3 деление
		number /= 2;
		binaryNumber += (number % 2) * ((long) Math.pow(10, power++));	// 4 деление
		number /= 2;
		binaryNumber += (number % 2) * ((long) Math.pow(10, power++));	// 5 деление
		number /= 2;
		binaryNumber += (number % 2) * ((long) Math.pow(10, power++));	// 6 деление
		number /= 2;
		binaryNumber += (number % 2) * ((long) Math.pow(10, power++));	// 7 деление
		number /= 2;
		binaryNumber += (number % 2) * ((long) Math.pow(10, power++));	// 8 деление
		number /= 2;
		binaryNumber += (number % 2) * ((long) Math.pow(10, power++));	// 9 деление
		number /= 2;
		binaryNumber += (number % 2) * ((long) Math.pow(10, power++));	// 10 деление
		number /= 2;
		binaryNumber += (number % 2) * ((long) Math.pow(10, power++));	// 11 деление
		number /= 2;
		binaryNumber += (number % 2) * ((long) Math.pow(10, power++));	// 12 деление
		number /= 2;
		binaryNumber += (number % 2) * ((long) Math.pow(10, power++));	// 13 деление
		number /= 2;
		binaryNumber += (number % 2) * ((long) Math.pow(10, power++));	// 14 деление
		number /= 2;
		binaryNumber += (number % 2) * ((long) Math.pow(10, power++));	// 15 деление
		number /= 2;
		binaryNumber += (number % 2) * ((long) Math.pow(10, power++));	// 16 деление
		number /= 2;
		binaryNumber += (number % 2) * ((long) Math.pow(10, power++));	// 17 деление

		System.out.println(binaryNumber);
	}
}