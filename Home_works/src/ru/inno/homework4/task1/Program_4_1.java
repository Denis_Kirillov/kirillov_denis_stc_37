package ru.inno.homework4.task1;

import java.util.Scanner;

class Program_4_1 {


	public static double isNumberPowerOfTwo(double n) {


		if (n == 2.0) {

			return  2.0;

		} else if (n > 2.0)  {

			return isNumberPowerOfTwo(n / 2.0);

		} else {

			return 1.0;
		}


	}


	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter number to check :");
		double n = scanner.nextDouble();

		if (isNumberPowerOfTwo(n) == 2.0) {

			System.out.println("Number " + (long) n + " is power of 2");

		} else {

			System.out.println("Number " + (long) n + " is NOT power of 2");

		}

	}

}