package ru.inno.homework4.task3;

import java.util.Scanner;

class Program_4_3 {


	public static long f(long[] array, int n) {

		System.out.println("--> f (" + n + ")");

		if (n == 1 || n == 2) {

			System.out.println("<-- f (" + n + ") = 1");
			return 1;

		}

		array[n] = f(array, n-1) + array[n-2];													// рекурсия считает только первый член суммы, второй добавляется 
		System.out.println("<-- f (" + n + ") = " + array[n] );									// массивом без вызова уже посчитанных рекурсий при первом прохождении
		return array[n];

	}


	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter index of fibonacci number :");
		int n = scanner.nextInt();

		long array[] = new long[n + 1];															// Массив для сохранения чисел фибоначчи посчитанных рекурсией

		array[1] = 1;
		array[2] = 1;
		System.out.println("Fibonacci number with index = " + n + " is: " + f(array, n));	

	}
}