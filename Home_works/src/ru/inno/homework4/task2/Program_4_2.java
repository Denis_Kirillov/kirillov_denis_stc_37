package ru.inno.homework4.task2;

import java.util.Scanner;
import java.util.Arrays;

class Program_4_2 {


	public static int binarySearch(int array[], int element, int left, int right, int middle) {

		if (left <= right) {																		// Цикл до тех пор пока границы не пересеклись	

			if (array[middle] == element) {

				return middle;

			} else if (array[middle] < element) {

				left = middle + 1;
				middle = left + (right - left) / 2;
				return binarySearch(array, element, left, right, middle);							// Возврат значения индекса элемента при успешном поиске

			} else if (array[middle] > element) {

				right = middle - 1;
				middle = left + (right - left) / 2;
				return binarySearch(array, element, left, right, middle);

			}

		}

		return -1;
		
	}


	public static int[] getArray() {																// инициализация алгоритма объявления массива

		Scanner scanner = new Scanner(System.in);												
		System.out.print("Enter array size: ");
		int size = scanner.nextInt();															
		int array[] = new int[size];															

		System.out.println("Enter array elements: ");

		for (int i = 0; i < array.length; i++) {												

			array[i] = scanner.nextInt();

		}

		return array;																			
	}


	public static void bubbleSort(int[] array) {													// инициализация алгоритма сортировки массива пузырьком
												
		int arrayBuf = 0;																		


		for (int i = 1; i < array.length; i++) {												

			for (int j = 0; j < array.length - i; j++) {

				if (array[j] > array[j+1]) {

					arrayBuf = array[j+1];						
					array[j+1] = array[j];
					array[j] = arrayBuf;

				}

			}

		}

		System.out.println("Sorted array: " + Arrays.toString(array));
		
	}


	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		int array[] = getArray();
		int left = 0;
		int right = array.length - 1;
		int middle = left + (right - left) / 2;

		System.out.println("Your array is :" + Arrays.toString(array));
		bubbleSort(array);
		System.out.println("Enter number to find :");
		int element = scanner.nextInt();

		int index = binarySearch(array, element, left, right, middle);


		if ( index == -1) {

			System.out.println("There is no such element");

		} else {

			System.out.println("Your element's index is " + (index + 1));

		}
		
	}

}