package ru.inno.game.server;

import ru.inno.game.dto.StatisticDto;
import ru.inno.game.services.GameService;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class GameServer {
    private ClientThread firstPlayer;
    private ClientThread secondPlayer;
    private ServerSocket serverSocket;

    private boolean isGameStarted = false;
    private boolean isGameInProcess = true;
    private long startTimeMills;
    private long gameId;
    private GameService gameService;
    private Lock lock = new ReentrantLock();

    public GameServer(GameService gameService){
        this.gameService = gameService;
    }

    public void start(int port){
        try {
            serverSocket = new ServerSocket(port);
            System.out.println("Сервер запущен . . .");
            System.out.println("Ожидание первого игрока . . .");
            firstPlayer = connect();
            System.out.println("Ожидание второго игрока . . .");
            secondPlayer = connect();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private ClientThread connect(){
        Socket client;

        try {
            client = serverSocket.accept();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

        ClientThread clientThread = new ClientThread(client);
        clientThread.start();
        System.out.println("Игрок подключен . . .");
        clientThread.sendMessage("Вы подключены к серверу");
        return clientThread;
    }

    private class ClientThread extends Thread{
        private final PrintWriter toClient;
        private final BufferedReader fromClient;
        private String playerNickname;
        private String ip;

        public ClientThread(Socket client){
            try {
                this.toClient = new PrintWriter(client.getOutputStream(),true);
                this.fromClient = new BufferedReader(new InputStreamReader(client.getInputStream()));
                this.ip = client.getInetAddress().getHostAddress();
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }

        @Override
        public void run() {
            while (isGameInProcess){
                String messageFromClient;

                try {
                    messageFromClient = fromClient.readLine();
                } catch (IOException e) {
                    throw new IllegalStateException(e);
                }

                if (messageFromClient != null) {
                    if (isMessageForNickname(messageFromClient)) {
                        resolveNickname(messageFromClient);
                    } else if (isMessageForMove(messageFromClient)){
                        resolveMove(messageFromClient);
                    } else if (isMessageForShot(messageFromClient)){
                        resolveShot(messageFromClient);
                    } else if(isMessageForDamage(messageFromClient)){
                        resolveDamage();
                    } else if (isMessageForExit(messageFromClient)){

                        lock.lock();
                        if (isGameInProcess) {
                            StatisticDto statistic = gameService.finishGame(gameId, (System.currentTimeMillis() - startTimeMills) / 1000);
                            firstPlayer.sendMessage("Stat: " + statistic);
                            secondPlayer.sendMessage("Stat: " + statistic);
                            isGameInProcess = false;
                        }
                        lock.unlock();
                    }
                }

                lock.lock();
                if (isReadyForStart()){
                    gameId = gameService.startGame(firstPlayer.getIp(), secondPlayer.getIp(),
                            firstPlayer.playerNickname, secondPlayer.playerNickname);
                    startTimeMills = System.currentTimeMillis();
                    isGameStarted = true;
                }
                lock.unlock();
            }
        }

        private boolean isMessageForNickname(String message){
            return message.startsWith("name: ");
        }

        private boolean isMessageForExit(String message){
            return message.equals("exit");
        }

        private boolean isMessageForShot(String message){
            return message.equals("shot");
        }

        private boolean isMessageForMove(String message){
            return (message.equals("left") || message.equals("right"));
        }

        private boolean isMessageForDamage(String message) {
            return message.equals("Damage");
        }

        private boolean isReadyForStart(){
            return (firstPlayer.playerNickname != null && secondPlayer.playerNickname != null && !isGameStarted);
        }

        private void resolveMove(String message){
            if (meFirst()){
                secondPlayer.sendMessage(message);
            } else {
                firstPlayer.sendMessage(message);
            }
        }

        private void resolveShot(String message){
            if (meFirst()){
                secondPlayer.sendMessage(message);
            } else {
                firstPlayer.sendMessage(message);
            }
        }

        private void resolveDamage() {
            if (meFirst()){
                gameService.shot(gameId, firstPlayer.playerNickname, secondPlayer.playerNickname);
            } else {
                gameService.shot(gameId, secondPlayer.playerNickname, firstPlayer.playerNickname);
            }
        }

        private void resolveNickname(String message){
            if (meFirst()){
                fixNickname(message, firstPlayer, "От первого игрока", secondPlayer);
            } else {
                fixNickname(message, secondPlayer, "От второго игрока", firstPlayer);
            }
        }

        private void fixNickname(String message, ClientThread currentClient, String prefix, ClientThread anotherClient){
            currentClient.playerNickname = message.substring(6);
            System.out.println(prefix + message);
            anotherClient.sendMessage(message);
        }

        public void sendMessage(String message){
            toClient.println(message);
        }

        boolean meFirst(){
            return this == firstPlayer;
        }

        public String getIp() {
            return ip;
        }
    }
}
