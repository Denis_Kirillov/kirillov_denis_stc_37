package ru.inno.game.app;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import ru.inno.game.repository.*;
import ru.inno.game.server.GameServer;
import ru.inno.game.services.GameService;
import ru.inno.game.services.GameServiceImpl;

import javax.sql.DataSource;

public class MainServer {
    public static void main(String[] args) {

        HikariConfig configuration = new HikariConfig();
        configuration.setJdbcUrl("jdbc:postgresql://localhost:5432/stc_37");
        configuration.setDriverClassName("org.postgresql.Driver");
        configuration.setUsername("postgres");
        configuration.setPassword("Java_stc_37");
        configuration.setMaximumPoolSize(20);

        DataSource dataSource = new HikariDataSource(configuration);

        PlayersRepository playersRepository = new PlayersRepositoryJDBCImpl(dataSource);
        GamesRepository gamesRepository = new GamesRepositoryJDBCImpl(dataSource);
        ShotsRepository shotsRepository = new ShotsRepositoryJDBCImpl(dataSource);

        GameService gameService = new GameServiceImpl(playersRepository, gamesRepository, shotsRepository);
        GameServer gameServer = new GameServer(gameService);
        gameServer.start(7777);
    }
}
