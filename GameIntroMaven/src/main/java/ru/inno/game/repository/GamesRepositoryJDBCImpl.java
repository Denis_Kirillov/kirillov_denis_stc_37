package ru.inno.game.repository;

import ru.inno.game.models.Game;
import ru.inno.game.models.Player;

import javax.sql.DataSource;
import java.sql.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


public class GamesRepositoryJDBCImpl implements GamesRepository {

    //language=SQL
    private static final String SQL_INSERT_GAME = "insert into game(time,first_player, second_player," +
            " first_player_shot_count, second_player_shot_count, amount_of_seconds) " +
            "values (?,?,?,?,?,?)";

    //language=SQL
    private static final String SQL_FIND_GAME_BY_ID = "select * from (select *, g.id as game_id, f.id as f_id," +
            " f.ip as f_ip, f.name as f_name, f.points as f_points, f.win_count as f_wc, f.lose_count as f_lc," +
            " s.id as s_id, s.ip as s_ip, s.name as s_name, s.points as s_points, s.win_count as s_wc," +
            " s.lose_count as s_lc " +
            "from game g " +
            "left join player f on g.first_player = f.id " +
            "left join player s on g.second_player = s.id) s " +
            "where game_id = ?";

    //language=SQL
    private static final String SQL_UPDATE_GAME_BY_ID = "update game set time = ?, first_player = ?, second_player = ?," +
            "first_player_shot_count = ?, second_player_shot_count = ?, amount_of_seconds = ?" +
            " where id = ?";

    private static final DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;

    private static final RowMapper<Game> gameRowMapper = row -> Game.builder()
            .id(row.getLong("game_id"))
            .dateTime(LocalDateTime.parse(row.getString("time"), formatter))
            .playerFirst(Player.builder()
                    .id(row.getLong("f_id"))
                    .ip(row.getString("f_ip"))
                    .name(row.getString("f_name"))
                    .points(row.getInt("f_points"))
                    .maxWinsCount(row.getInt("f_wc"))
                    .maxLosesCount(row.getInt("f_lc"))
                    .build())
            .playerSecond(Player.builder()
                    .id(row.getLong("s_id"))
                    .ip(row.getString("s_ip"))
                    .name(row.getString("s_name"))
                    .points(row.getInt("s_points"))
                    .maxWinsCount(row.getInt("s_wc"))
                    .maxLosesCount(row.getInt("s_lc"))
                    .build())
            .playerFirstShotsCount(row.getInt("first_player_shot_count"))
            .playerSecondShotsCount(row.getInt("second_player_shot_count"))
            .secondsGameTimeAmount(row.getLong("amount_of_seconds"))
            .build();

    DataSource dataSource;

    public GamesRepositoryJDBCImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void save(Game game) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT_GAME, Statement.RETURN_GENERATED_KEYS)) {

            setGame(game, statement);
            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't insert data");
            }

            try (ResultSet generatedId = statement.getGeneratedKeys()) {
                if (generatedId.next()) {
                    game.setId(generatedId.getLong("id"));
                } else {
                    throw new SQLException("Can't retrieve id");
                }
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }


    @Override
    public Game findById(Long gameId) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_GAME_BY_ID)) {

            statement.setLong(1, gameId);
            try (ResultSet rows = statement.executeQuery()) {
                if (rows.next()) {
                    return gameRowMapper.mapRow(rows);
                }
            }

            return null;
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }

    }

    @Override
    public void update(Game game) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_GAME_BY_ID)) {

            setGame(game, statement);
            statement.setLong(7, game.getId());
            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't insert data");
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    private void setGame(Game game, PreparedStatement statement) throws SQLException {
        statement.setString(1, game.getDateTime().format(formatter));
        statement.setLong(2, game.getPlayerFirst().getId());
        statement.setLong(3, game.getPlayerSecond().getId());
        statement.setInt(4, game.getPlayerFirstShotsCount());
        statement.setInt(5, game.getPlayerSecondShotsCount());
        statement.setLong(6, game.getSecondsGameTimeAmount());
    }
}
