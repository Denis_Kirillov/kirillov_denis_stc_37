package ru.inno.game.repository;

import ru.inno.game.models.Player;

import javax.sql.DataSource;
import java.sql.*;


public class PlayersRepositoryJDBCImpl implements PlayersRepository {

    //language=SQL
    private static final String SQL_INSERT_PLAYER = "insert into player(ip, points," +
            " win_count, lose_count, name) values (?,?,?,?,?)";

    //language=SQL
    private static final String SQL_FIND_PLAYER_BY_NICKNAME = "select player.id as player_id, * from player where name = ?";

    //language=SQL
    private static final String SQL_UPDATE_PLAYER_BY_NICKNAME = "update player set ip = ?, points = ?," +
            "win_count = ?, lose_count = ? where name = ?";

    private static final RowMapper<Player> playerRowMapper = row -> Player.builder()
            .id(row.getLong("id"))
            .ip(row.getString("ip"))
            .name(row.getString("name"))
            .points(row.getInt("points"))
            .maxWinsCount(row.getInt("win_count"))
            .maxLosesCount(row.getInt("lose_count"))
            .build();

    DataSource dataSource;

    public PlayersRepositoryJDBCImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }


    @Override
    public Player findByNickname(String nickname) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_PLAYER_BY_NICKNAME)) {

            statement.setString(1, nickname);

            try (ResultSet rows = statement.executeQuery()) {

                if (rows.next()) {
                    return playerRowMapper.mapRow(rows);
                }
            }

            return null;
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }


    @Override
    public void save(Player player) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT_PLAYER, Statement.RETURN_GENERATED_KEYS)) {

            setPlayer(player, statement);

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't insert data");
            }

            try (ResultSet generatedId = statement.getGeneratedKeys()) {

                if (generatedId.next()) {
                    player.setId(generatedId.getLong("id"));
                } else {
                    throw new SQLException("Can't retrieve id");
                }
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }


    @Override
    public void update(Player player) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_PLAYER_BY_NICKNAME)) {

            setPlayer(player, statement);

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't insert data");
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }


    private void setPlayer(Player player, PreparedStatement statement) throws SQLException {
        statement.setString(1, player.getIp());
        statement.setLong(2, player.getPoints());
        statement.setInt(3, player.getMaxWinsCount());
        statement.setInt(4, player.getMaxLosesCount());
        statement.setString(5, player.getName());
    }
}
