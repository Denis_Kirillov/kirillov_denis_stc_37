package ru.inno.game.repository;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * 11.04.2021
 * 36. DB
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface RowMapper<T> {
    T mapRow(ResultSet row) throws SQLException;
}
