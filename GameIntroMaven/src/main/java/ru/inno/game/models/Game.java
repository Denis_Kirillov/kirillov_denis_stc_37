package ru.inno.game.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * 21.03.2021
 * GameIntro
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Game {
    private Long id;
    private LocalDateTime dateTime;
    private Player playerFirst;
    private Player playerSecond;
    private Integer playerFirstShotsCount;
    private Integer playerSecondShotsCount;
    private Long secondsGameTimeAmount;
}
