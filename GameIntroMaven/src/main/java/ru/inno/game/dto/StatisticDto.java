package ru.inno.game.dto;

import ru.inno.game.repository.PlayersRepository;

/**
 * 25.03.2021
 * GameIntro
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// информация об игре
public class StatisticDto {
    private String firstPlayer;
    private Integer playerFirstShotsCount;
    private Integer playerSecondShotsCount;
    private String secondPlayer;
    private String winner;
    private Long gameID;
    private Long gameTime;


    private PlayersRepository playersRepository;

    public StatisticDto(String firstPlayer, String secondPlayer, Integer playerFirstShotsCount,
                        Integer playerSecondShotsCount, String winner, Long gameID,
                        Long gameTime, PlayersRepository playersRepository) {
        this.firstPlayer = firstPlayer;
        this.playerFirstShotsCount = playerFirstShotsCount;
        this.playerSecondShotsCount = playerSecondShotsCount;
        this.secondPlayer = secondPlayer;
        this.winner = winner;
        this.gameID = gameID;
        this.gameTime = gameTime;
        this.playersRepository = playersRepository;
    }

    @Override
    public String toString() {
        return "Игра с ID = " + gameID +
                "!" +
                "!Игрок 1: " + firstPlayer +
                ",!попаданий - " + playerFirstShotsCount +
                ",!всего очков - " + playersRepository.findByNickname(firstPlayer).getPoints() +
                "!" +
                "!Игрок 2: " + secondPlayer +
                ",!попаданий - " + playerSecondShotsCount +
                ",!всего очков - " + playersRepository.findByNickname(secondPlayer).getPoints() +
                "!" +
                "!Победа: " + winner +
                "!Игра длилась: " + gameTime + " секунд";
    }
}
