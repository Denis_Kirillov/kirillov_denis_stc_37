package ru.inno.game.services;

import ru.inno.game.dto.StatisticDto;
import ru.inno.game.models.Game;
import ru.inno.game.models.Player;
import ru.inno.game.models.Shot;
import ru.inno.game.repository.GamesRepository;
import ru.inno.game.repository.PlayersRepository;
import ru.inno.game.repository.ShotsRepository;

import java.time.LocalDateTime;

/**
 * 25.03.2021
 * GameIntro
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// бизнес-логика
public class GameServiceImpl implements GameService {
    private PlayersRepository playersRepository;
    private GamesRepository gamesRepository;
    private ShotsRepository shotsRepository;

    public GameServiceImpl(PlayersRepository playersRepository, GamesRepository gamesRepository, ShotsRepository shotsRepository) {
        this.playersRepository = playersRepository;
        this.gamesRepository = gamesRepository;
        this.shotsRepository = shotsRepository;
    }

    @Override
    public Long startGame(String firstIp, String secondIp, String firstPlayerNickname, String secondPlayerNickname) {
        // получили информацию об обоих игроках
        Player first = checkIfExists(firstIp, firstPlayerNickname);
        Player second = checkIfExists(secondIp, secondPlayerNickname);
        // создали игру
        Game game = Game.builder()
                .dateTime(LocalDateTime.now())
                .playerFirst(first)
                .playerSecond(second)
                .playerFirstShotsCount(0)
                .playerSecondShotsCount(0)
                .secondsGameTimeAmount(0L)
                .build();
        // сохранили игру в репозитории
        gamesRepository.save(game);
        return game.getId();
    }

    private Player checkIfExists(String ip, String nickname) {
        Player player = playersRepository.findByNickname(nickname);
        // если нет первого игрока под таким именем
        if (player == null) {
            // создаем игрока
            player = Player.builder()
                    .ip(ip)
                    .name(nickname)
                    .maxWinsCount(0)
                    .maxLosesCount(0)
                    .points(0)
                    .build();
            // сохраняем его в репозитории
            playersRepository.save(player);
        } else {
            // если такой игрок был -> обновляем у него IP-адрес
            player.setIp(ip);
            playersRepository.update(player);
        }

        return player;
    }

    @Override
    public void shot(Long gameId, String shooterNickname, String targetNickname) {
        // получаем того, кто стрелял из репозитория
        Player shooter = playersRepository.findByNickname(shooterNickname);
        // получаем того, в кого стреляли из репозитория
        Player target = playersRepository.findByNickname(targetNickname);
        // получаем игру
        Game game = gamesRepository.findById(gameId);
        // создаем выстрел
        Shot shot = Shot.builder()
                .dateTime(LocalDateTime.now())
                .game(game)
                .shooter(shooter)
                .target(target)
                .build();
        // увеличиваем очки у стреляющего
        shooter.setPoints(shooter.getPoints() + 1);
        // если стрелявший - первый игрок
        if (game.getPlayerFirst().getName().equals(shooterNickname)) {
            // сохраняем информацию о выстреле в игре
            game.setPlayerFirstShotsCount(game.getPlayerFirstShotsCount() + 1);
        } else if (game.getPlayerSecond().getName().equals(shooterNickname)) {
            // сохраняем информацию о выстреле в игре
            game.setPlayerSecondShotsCount(game.getPlayerSecondShotsCount() + 1);
        }
        // обновляем данные по стреляющему
        playersRepository.update(shooter);
        // обновляем данные по игре
        gamesRepository.update(game);
        // сохраняем выстрел
        shotsRepository.save(shot);
    }

    @Override
    public StatisticDto finishGame(Long gameId, long timeSecondsAmount) {
        Game game = gamesRepository.findById(gameId);
        String winner = "Ничья";

        game.setSecondsGameTimeAmount(timeSecondsAmount);
        Player first = playersRepository.findByNickname(game.getPlayerFirst().getName());
        Player second = playersRepository.findByNickname(game.getPlayerSecond().getName());

        if (game.getPlayerFirstShotsCount() > game.getPlayerSecondShotsCount()) {                   // Определение победителя
            winner = resolveWinner(first, second);
        } else if (game.getPlayerFirstShotsCount() < game.getPlayerSecondShotsCount()) {
            winner = resolveWinner(second, first);
        }

        playersRepository.update(first);
        playersRepository.update(second);
        gamesRepository.update(game);

        return new StatisticDto(first.getName(), second.getName(), game.getPlayerFirstShotsCount(),
                game.getPlayerSecondShotsCount(), winner, gameId, timeSecondsAmount, playersRepository);
    }

    public String resolveWinner(Player whoWin, Player whoLose) {
        whoWin.setMaxWinsCount(whoWin.getMaxWinsCount() + 1);
        whoLose.setMaxLosesCount(whoLose.getMaxLosesCount() + 1);
        return whoWin.getName();
    }

}
